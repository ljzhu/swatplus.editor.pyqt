.. _swatplus-editor-design:

SWAT+ Editor Design
===================

SWAT+ Editor is a program that allows users to modify SWAT+ inputs easily without having to touch the SWAT+ input text
files directly. The editor will import a watershed created in QSWAT+ or ArcSWAT+, or allow the user to create a SWAT+
project from scratch. The user may write input files and run the SWAT+ model through the editor.

Technologies
------------

The following software is used to create and build SWAT+ Editor:

* `Python 3.x <https://www.python.org/>`_
* `PyQt 5 <https://riverbankcomputing.com/software/pyqt>`_
* `PyInstaller <http://www.pyinstaller.org/>`_
* `SQLite <https://www.sqlite.org/>`_
* `Peewee ORM <http://docs.peewee-orm.com>`_
* `Git repository hosted on Bitbucket <https://bitbucket.org/swatplus/swatplus.editor>`_
* `Read the Docs <https://readthedocs.org/>`_

Database Design
---------------

SWAT+ Editor uses a `SQLite <https://www.sqlite.org/>`_ database to hold model input data to allow easy manipulation by the user. The database is
structured to closely resemble the SWAT+ ASCII text files in order to keep a clean link between the model and editor.
The following conventions are used in the project database:

* The table names will match the text file names, replacing any “.” or “-“ with an underscore “_”.
* The table column names will match the model's variable names. All names use lowercase and underscores.
* Any text file with a variable number of repetitive columns will use a related table in the database. For example, many of the connection files contain a variable number of repeated outflow connection columns (obtyp_out, obtyno_out, hytyp_out, frac_out). In the database, we represent these in a separate table, basically transposing a potentially long horizontal file to columns.
* All tables will use a numeric "id" as the primary key, and foreign key relationships will use these integer ids instead of a text name. This will allow for easier modification of these object names by the user and help keep the database size down for large projects.

A separate SQLite database containing common datasets and input metadata will be provided with SWAT+ Editor.
(This is a replacement for the SWAT2012.mdb packaged with SWAT2012 versions of ArcSWAT, QSWAT, and SWATeditor.)

In addition, reformatted SSURGO and STATSGO soils databases will be available for download. The structure of the soils
database has been split into two tables: a soil table and soil_layer table.

Similarly, the global weather weather generator database will be available for download in SQLite format. The structure
of the wgn database has been split into two tables: a wgn table and wgn_monthly_value table.

A tool will be provided to convert old soils and wgn tables to the new format.

SWAT+ Documentation
-------------------

SWAT+ is the latest version of the `SWAT model <http://swat.tamu.edu>`_, which is a command line executable written in Fortran.
It uses several ASCII text files as inputs to the model. The documentation for these files is still under development.

* `SWAT+ code repository on Bitbucket <https://bitbucket.org/blacklandgrasslandmodels/modular_swatplus>`_
