Database Documentation
======================

Project Database
----------------

The project database in SWAT+ Editor is made in `SQLite <https://www.sqlite.org/>`_.
SWAT+ Editor uses the `Peewee ORM <http://docs.peewee-orm.com>`_ (`object-relational mapping <https://en.wikipedia.org/wiki/Object-relational_mapping>`_)
to represent and work with the tables in Python. The use of an ORM provides a layer of abstraction and portability
in hopes of streamlining future SWAT+ development projects.

`Download a SQLite file containing the project table structure. <https://bitbucket.org/swatplus/swatplus.editor/downloads/swatplus_empty_project.sqlite>`_

.. note:: This page is incomplete and under development.

Foreign Keys
************

Relationships are defined in a `Peewee ORM <http://docs.peewee-orm.com>`_ python class as a :code:`ForeignKeyField`.
In the python class, the field will be named after the object it is referencing. In the database, this name will
automatically be appended by the referencing table's column name, which is usually :code:`id`.

For example, we have two tables representing soils: soils (:code:`soils_sol`) and layers (:code:`soils_sol_layer`). The layer table has a
foreign key to the main soils table, so we know to which soil the layer belongs. In the python class, this field is named
:code:`soil`, and in the database it is called :code:`soil_id`. See the :ref:`Soils module <soils>` for reference.

Modules and Tables
******************

A Python module is created for each input classification, based on the SWAT+ file.cio input file:

* :ref:`simulation`
* :ref:`climate`
* :ref:`connect`
* :ref:`channel`
* :ref:`reservoir`
* :ref:`routing-unit`
* :ref:`hru`
* :ref:`dr`
* :ref:`aquifer`
* :ref:`herd`
* :ref:`water-rights`
* :ref:`link`
* :ref:`basin`
* :ref:`hydrology`
* :ref:`exco`
* :ref:`bacteria`
* :ref:`structural`
* :ref:`parm-db`
* :ref:`ops`
* :ref:`lum`
* :ref:`update`
* :ref:`init`
* :ref:`soils`
* :ref:`decision-table`
* :ref:`constituents`
* :ref:`regions`

.. _simulation:

Simulation
**********

+-------------------------------+------------------------+
| **Section label in file.cio** | SIMULATION             |
+-------------------------------+------------------------+
| **Python module**             | database/simulation.py |
+-------------------------------+------------------------+

Tables
++++++

+----------------+---------------------+------------------------------------+
| Text file name | Database table name | Related tables                     |
+================+=====================+====================================+
| time.sim       | time_sim            |                                    |
+----------------+---------------------+------------------------------------+
| print.prt      | print_prt           | print_prt_aa_int, print_prt_object |
+----------------+---------------------+------------------------------------+
| object.prt     | object_prt          |                                    |
+----------------+---------------------+------------------------------------+
| object.cnt     | object_cnt          |                                    |
+----------------+---------------------+------------------------------------+

Time_sim Source
+++++++++++++++

.. code-block:: python

    class Time_sim(BaseModel):
        jd_start = IntegerField()
        yr_start = IntegerField()
        jd_end = IntegerField()
        yr_end = IntegerField()
        step = IntegerField()

+----+----------+-----+
| time_sim            |
+====+==========+=====+
| PK | id       | int |
+----+----------+-----+
|    | jd_start | int |
+----+----------+-----+
|    | yr_start | int |
+----+----------+-----+
|    | jd_end   | int |
+----+----------+-----+
|    | idal_in  | int |
+----+----------+-----+
|    | step     | int |
+----+----------+-----+

.. _climate:

Climate
*******

.. _connect:

Connect
*******

.. _channel:

Channel
*******

.. _reservoir:

Reservoir
*********

.. _routing-unit:

Routing Unit
************

.. _hru:

HRU
***

.. _dr:

Delivery Ratio
**************

.. _aquifer:

Aquifer
*******

.. _herd:

Herd
****

.. _water-rights:

Water Rights
************

.. _link:

Link
****

.. _basin:

Basin
*****

.. _hydrology:

Hydrology
*********

.. _exco:

Exco
****

.. _bacteria:

Bacteria
********

.. _structural:

Structural
**********

.. _parm-db:

Parameter Database
******************

.. _ops:

Operations
**********

.. _lum:

Landuse Management
******************

.. _update:

Update
******

.. _init:

Init
****

.. _soils:

Soils
*****

+-------------------------------+-------------------+
| **Section label in file.cio** | SOILS             |
+-------------------------------+-------------------+
| **Python module**             | database/soils.py |
+-------------------------------+-------------------+

Tables
++++++

+----------------+---------------------+-----------------+
| Text file name | Database table name | Related tables  |
+================+=====================+=================+
| soils.sol      | soils_sol           | soils_sol_layer |
+----------------+---------------------+-----------------+
| nutrients.sol  | nutrients_sol       |                 |
+----------------+---------------------+-----------------+

References
++++++++++

+--------------+-------------------+------------------+----------------+
| Table        | Foreign key       | Referenced table | Ref. table key |
+==============+===================+==================+================+
| hru_data_hru | soil_id           | soils_sol        | id             |
+--------------+-------------------+------------------+----------------+
| hru_data_hru | soil_nutr_init_id | nutrients_sol    | id             |
+--------------+-------------------+------------------+----------------+

Soils_sol Source
++++++++++++++++

The soils represented in this table are the soils used in the user's model. They are populated from an external table
of soils formatted in a similar structure to the one below. US SSURGO and STATSGO (and potentially other global soils)
SQLite databases are available on the `SWAT website <http://swat.tamu.edu>`_. A tool will be provided to convert
SWAT2012 soil databases to the new format.

.. code-block:: python

    class Soils_sol(BaseModel):
        name = CharField(unique=True)
        hydgrp = CharField()
        zmx = DoubleField()
        anion_excl = DoubleField()
        crk = DoubleField()
        texture = CharField()
        desc = TextField(null=True)

+----+------------+------+--------+
| soils_sol                       |
+====+============+======+========+
| PK | id         | int  |        |
+----+------------+------+--------+
|    | name       | text | unique |
+----+------------+------+--------+
|    | hydgrp     | text |        |
+----+------------+------+--------+
|    | zmx        | real |        |
+----+------------+------+--------+
|    | anion_excl | real |        |
+----+------------+------+--------+
|    | crk        | real |        |
+----+------------+------+--------+
|    | texture    | text |        |
+----+------------+------+--------+
|    | desc       | text |        |
+----+------------+------+--------+

Soils_sol_layer Source
++++++++++++++++++++++

Each soil can have many layers. In SWAT2012, the layers were represented in a single table along with the soil
definition. In SWAT+ we have opted for a `normalized approach <https://en.wikipedia.org/wiki/Database_normalization>`_.

.. code-block:: python

    class Soils_sol_layer(BaseModel):
        soil = ForeignKeyField(Soils_sol, on_delete='CASCADE', related_name='layers')
        layer_num = IntegerField()
        z = DoubleField()
        bd = DoubleField()
        awc = DoubleField()
        k = DoubleField()
        cbn = DoubleField()
        clay = DoubleField()
        silt = DoubleField()
        sand = DoubleField()
        rock = DoubleField()
        alb = DoubleField()
        usle_k = DoubleField()
        ec = DoubleField()
        cal = DoubleField()
        ph = DoubleField()

+----+------------+------+
| soils_sol_layer        |
+====+============+======+
| PK | id         | int  |
+----+------------+------+
| FK | soil_id    | int  |
+----+------------+------+
|    | layer_num  | int  |
+----+------------+------+
|    | z          | real |
+----+------------+------+
|    | bd         | real |
+----+------------+------+
|    | awc        | real |
+----+------------+------+
|    | k          | real |
+----+------------+------+
|    | cbn        | real |
+----+------------+------+
|    | clay       | real |
+----+------------+------+
|    | silt       | real |
+----+------------+------+
|    | sand       | real |
+----+------------+------+
|    | rock       | real |
+----+------------+------+
|    | alb        | real |
+----+------------+------+
|    | usle_k     | real |
+----+------------+------+
|    | ec         | real |
+----+------------+------+
|    | cal        | real |
+----+------------+------+
|    | ph         | real |
+----+------------+------+

Nutrients_sol Source
++++++++++++++++++++

.. code-block:: python

    class Nutrients_sol(BaseModel):
        name = CharField(unique=True)
        exp_co = DoubleField()
        totaln = DoubleField()
        inorgn = DoubleField()
        orgn = DoubleField()
        totalp = DoubleField()
        inorgp = DoubleField()
        orgp = DoubleField()
        watersol_p = DoubleField()
        h3a_p = DoubleField()
        mehlich_p = DoubleField()
        bray_strong_p = DoubleField()
        desc = TextField(null=True)

+----+---------------+------+--------+
| nutrients_sol                      |
+====+===============+======+========+
| PK | id            | int  |        |
+----+---------------+------+--------+
|    | name          | text | unique |
+----+---------------+------+--------+
|    | exp_co        | real |        |
+----+---------------+------+--------+
|    | totaln        | real |        |
+----+---------------+------+--------+
|    | inorgn        | real |        |
+----+---------------+------+--------+
|    | orgn          | real |        |
+----+---------------+------+--------+
|    | totalp        | real |        |
+----+---------------+------+--------+
|    | inorgp        | real |        |
+----+---------------+------+--------+
|    | orgp          | real |        |
+----+---------------+------+--------+
|    | watersol_p    | real |        |
+----+---------------+------+--------+
|    | h3a_p         | real |        |
+----+---------------+------+--------+
|    | mehlich_p     | real |        |
+----+---------------+------+--------+
|    | bray_strong_p | real |        |
+----+---------------+------+--------+
|    | desc          | text |        |
+----+---------------+------+--------+

.. _decision-table:

Decision Table
**************

.. _constituents:

Constituents
************

.. _regions:

Regions
*******
