.. SWAT+ Editor documentation master file, created by
   sphinx-quickstart on Thu Mar 23 11:13:58 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

SWAT+ Editor
============

Python interface to SWAT+ allowing the user to import a project from GIS, modify SWAT+ input, write the text files, and run the model.

SWAT+ Resources
---------------
* `SWAT+ Editor source code repository <https://bitbucket.org/swatplus/swatplus.editor>`_
* `SWAT+ source code repository maintained by Blackland <https://bitbucket.org/blacklandgrasslandmodels/modular_swatplus>`_

Contents:
---------
.. toctree::
   :maxdepth: 2
   :caption: About SWAT+ Editor

   design

.. toctree::
   :maxdepth: 2
   :caption: User Docs

   user/sqlite

.. toctree::
   :maxdepth: 2
   :caption: Developer Docs

   developer/database
