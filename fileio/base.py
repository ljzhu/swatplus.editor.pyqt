from abc import ABCMeta, abstractmethod
from peewee import *
import time
import ntpath
import utils


class BaseFileModel:
    __metaclass__ = ABCMeta

    def __init(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    @abstractmethod
    def read(self): pass

    @abstractmethod
    def write(self): pass

    def check_cols(self, cols, num_expecting, file_name):
        if len(cols) < num_expecting:
            raise IndexError(
                'Improperly formatted %s file. Expecting %s columns. Please refer to the SWAT+ IO documentation.' % (file_name, num_expecting))

    def get_meta_line(self, alt_file_name=""):
        vtxt = ""
        if self.version is not None:
            vtxt = " v{version}".format(version=self.version)

        name = self.file_name if alt_file_name == "" else alt_file_name

        return "{file}: written by SWAT+ editor{version} on {date}\n".format(file=ntpath.basename(name), version=vtxt, date=time.strftime("%Y-%m-%d %H:%M"))

    def write_meta_line(self, file, alt_file_name=""):
        file.write(self.get_meta_line(alt_file_name=alt_file_name))

    def write_headers(self, file, cols):
        for file_col in cols:
            if file_col.not_in_db:
                if file_col.padding_override is not None:
                    utils.write_string(file, file_col.value, default_pad=file_col.padding_override, direction=file_col.direction)
                else:
                    utils.write_string(file, file_col.value, direction=file_col.direction)
            else:
                db_col = file_col.value
                name = db_col.name

                if not file_col.is_desc:
                    if file_col.alt_header_name != "":
                        name = file_col.alt_header_name
                    elif file_col.query_alias != "":
                        name = file_col.query_alias
                    elif db_col.verbose_name != "":
                        name = db_col.verbose_name
                    name = name.upper()

                    if file_col.repeat is not None:
                        name += str(file_col.repeat)

                    if file_col.padding_override is not None:
                        utils.write_string(file, name, default_pad=file_col.padding_override, direction=file_col.direction)
                    elif isinstance(db_col, ForeignKeyField):
                        utils.write_string(file, name, direction=file_col.direction)
                    elif isinstance(db_col, IntegerField):
                        utils.write_int(file, name, direction=file_col.direction)
                    elif isinstance(db_col, DoubleField):
                        utils.write_num(file, name, direction=file_col.direction)
                    else:
                        utils.write_string(file, name, direction=file_col.direction)

    def write_row(self, file, cols):
        for file_col in cols:
            string_null = file_col.text_if_null if file_col.text_if_null is not None else utils.NULL_STR
            num_null = file_col.text_if_null if file_col.text_if_null is not None else utils.NULL_NUM

            if file_col.is_desc:
                utils.write_desc_string(file, file_col.value)
            elif file_col.padding_override is not None:
                utils.write_string(file, file_col.value, default_pad=file_col.padding_override, direction=file_col.direction, text_if_null=string_null)
            elif isinstance(file_col.value, int):
                utils.write_int(file, file_col.value, direction=file_col.direction)
            elif isinstance(file_col.value, float):
                utils.write_num(file, file_col.value, direction=file_col.direction, text_if_null=num_null)
            else:
                utils.write_string(file, file_col.value, direction=file_col.direction, text_if_null=string_null)

    def write_table(self, table, cols):
        self.write_query(table.select().order_by(table.id), cols)

    def write_default_table(self, table):
        cols = []
        for field in table._meta.sorted_fields:
            col = FileColumn(field)

            if field.name == "desc":
                col = FileColumn(field, is_desc=True)
            elif field.name == "name":
                col = FileColumn(field, direction="left")

            cols.append(col)

        self.write_table(table, cols)

    def write_query(self, query, cols):
        if query.count() > 0:
            with open(self.file_name, 'w') as file:
                self.write_meta_line(file)
                self.write_headers(file, cols)
                file.write("\n")

                for row in query.dicts():
                    row_cols = []
                    for col in cols:
                        col_name = col.query_alias if col.query_alias != "" else col.value.name
                        row_cols.append(FileColumn(row[col_name], direction=col.direction, padding_override=col.padding_override, text_if_null=col.text_if_null, is_desc=col.is_desc))

                    self.write_row(file, row_cols)
                    file.write("\n")


class FileColumn:
    def __init__(self, value, direction="right", padding_override=None, not_in_db=False, repeat=None, alt_header_name="", query_alias="", text_if_null=None, is_desc=False):
        self.value = value
        self.direction = direction
        self.padding_override = padding_override
        self.not_in_db = not_in_db
        self.repeat = repeat
        self.alt_header_name = alt_header_name
        self.query_alias = query_alias
        self.text_if_null = text_if_null
        self.is_desc = is_desc
