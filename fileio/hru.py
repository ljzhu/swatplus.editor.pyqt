from .base import BaseFileModel
import utils
import database.project.hru as db


class Hru_data_hru(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Hru_data_hru
        order_by = db.Hru_data_hru.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.int_pad("HRU_NUMB"))
                file.write(utils.string_pad("HRU_NAME", direction="left"))
                file.write(utils.string_pad("HRUTOPO"))
                file.write(utils.string_pad("HYDRO"))
                file.write(utils.string_pad("SOIL"))
                file.write(utils.string_pad("LU_MGT"))
                file.write(utils.string_pad("SOILNUT_INIT"))
                file.write(utils.string_pad("SURF_STOR"))
                file.write(utils.string_pad("SNOW"))
                file.write(utils.string_pad("FIELD"))
                file.write("\n")

                for row in table.select().order_by(order_by):
                    file.write(utils.int_pad(row.id))
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write(utils.key_name_pad(row.topo))
                    file.write(utils.key_name_pad(row.hyd))
                    file.write(utils.key_name_pad(row.soil))
                    file.write(utils.key_name_pad(row.land_use_mgt))
                    file.write(utils.key_name_pad(row.soil_nutr_init))
                    file.write(utils.string_pad(row.surf_stor))
                    file.write(utils.key_name_pad(row.snow))
                    file.write(utils.string_pad(row.field))
                    file.write("\n")
