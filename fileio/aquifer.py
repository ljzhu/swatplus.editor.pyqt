from .base import BaseFileModel, FileColumn as col
import utils
import database.project.aquifer as db


class Aquifer_aqu(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        self.write_default_table(db.Aquifer_aqu)