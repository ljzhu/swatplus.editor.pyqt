from .base import BaseFileModel, FileColumn as col
from peewee import *
import utils
import database.project.climate as db


class Weather_sta_cli(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Weather_sta_cli
        query = (table.select(table.id,
                              table.name,
                              db.Weather_wgn_cli.name.alias("wgn"),
                              table.pcp,
                              table.tmp,
                              table.slr,
                              table.hmd,
                              table.wnd,
                              table.wnd_dir,
                              table.atmo_dep)
                      .join(db.Weather_wgn_cli, JOIN.LEFT_OUTER)
                      .order_by(table.name))

        cols = [col(table.id, alt_header_name="wst_numb"),
                col(table.name, direction="left"),
                col(table.wgn, query_alias="wgn"),
                col(table.pcp, padding_override=25),
                col(table.tmp, padding_override=25),
                col(table.slr, padding_override=25),
                col(table.hmd, padding_override=25),
                col(table.wnd, padding_override=25),
                col(table.wnd_dir),
                col(table.atmo_dep)]
        self.write_query(query, cols)


class Weather_wgn_cli(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        wgn = db.Weather_wgn_cli.select().order_by(db.Weather_wgn_cli.name)
        months = db.Weather_wgn_cli_mon.select().order_by(db.Weather_wgn_cli_mon.month)
        query = prefetch(wgn, months)

        if wgn.count() > 0:
            with open(self.file_name, 'w') as file:
                self.write_meta_line(file)
                header_cols = [col(db.Weather_wgn_cli.id, alt_header_name="numb"),
                               col(db.Weather_wgn_cli.name, direction="left", padding_override=25),
                               col(db.Weather_wgn_cli.lat),
                               col(db.Weather_wgn_cli.lon),
                               col(db.Weather_wgn_cli.elev),
                               col(db.Weather_wgn_cli.rain_yrs)]
                self.write_headers(file, header_cols)

                mt = db.Weather_wgn_cli_mon
                for i in range(1, 13):
                    mon_cols = [col(mt.tmp_max_ave, repeat=i),
                                col(mt.tmp_min_ave, repeat=i),
                                col(mt.tmp_max_sd, repeat=i),
                                col(mt.tmp_min_sd, repeat=i),
                                col(mt.pcp_ave, repeat=i),
                                col(mt.pcp_sd, repeat=i),
                                col(mt.pcp_skew, repeat=i),
                                col(mt.wet_dry, repeat=i),
                                col(mt.wet_wet, repeat=i),
                                col(mt.pcp_days, repeat=i),
                                col(mt.pcp_hhr, repeat=i),
                                col(mt.slr_ave, repeat=i),
                                col(mt.dew_ave, repeat=i),
                                col(mt.wnd_ave, repeat=i)]
                    self.write_headers(file, mon_cols)

                file.write("\n")

                i = 1
                for row in query:
                    row_cols = [col(i),
                                col(row.name, direction="left", padding_override=25),
                                col(row.lat),
                                col(row.lon),
                                col(row.elev),
                                col(row.rain_yrs)]
                    self.write_row(file, row_cols)
                    i += 1

                    for month in row.monthly_values_prefetch:
                        month_row_cols = [col(month.tmpmx),
                                          col(month.tmpmn),
                                          col(month.tmpstdmx),
                                          col(month.tmpstdmn),
                                          col(month.pcpmm),
                                          col(month.pcpstd),
                                          col(month.pcpskw),
                                          col(month.pr_wd),
                                          col(month.pr_ww),
                                          col(month.pcpd),
                                          col(month.rainhhmx),
                                          col(month.solarav),
                                          col(month.dewpt),
                                          col(month.wndav)]
                        self.write_row(file, month_row_cols)

                    file.write("\n")
