from .base import BaseFileModel, FileColumn as col
from peewee import *
import database.project.channel as db
import database.project.link as link


class Initial_cha(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        self.write_default_table(db.Initial_cha)


class Hydrology_cha(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        self.write_default_table(db.Hydrology_cha)


class Sediment_cha(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        self.write_default_table(db.Sediment_cha)


class Nutrients_cha(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        self.write_default_table(db.Nutrients_cha)


class Pesticide_cha(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        self.write_default_table(db.Pesticide_cha)


class Channel_cha(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Channel_cha
        query = (table.select(table.id,
                              table.name,
                              db.Initial_cha.name.alias("cha_ini"),
                              db.Hydrology_cha.name.alias("cha_hyd"),
                              db.Sediment_cha.name.alias("cha_sed"),
                              db.Nutrients_cha.name.alias("cha_nut"),
                              db.Pesticide_cha.name.alias("cha_pst"),
                              link.Chan_surf_lin.name.alias("cha_ls_lnk"),
                              link.Chan_aqu_lin.name.alias("cha_aqu_lnk"))
                      .join(db.Initial_cha, JOIN.LEFT_OUTER)
                      .switch(table)
                      .join(db.Hydrology_cha, JOIN.LEFT_OUTER)
                      .switch(table)
                      .join(db.Sediment_cha, JOIN.LEFT_OUTER)
                      .switch(table)
                      .join(db.Nutrients_cha, JOIN.LEFT_OUTER)
                      .switch(table)
                      .join(db.Pesticide_cha, JOIN.LEFT_OUTER)
                      .switch(table)
                      .join(link.Chan_surf_lin, JOIN.LEFT_OUTER)
                      .switch(table)
                      .join(link.Chan_aqu_lin, JOIN.LEFT_OUTER)
                      .order_by(table.id))

        cols = [col(table.id),
                col(table.name, direction="left"),
                col(table.init, query_alias="cha_ini"),
                col(table.hyd, query_alias="cha_hyd"),
                col(table.sed, query_alias="cha_sed"),
                col(table.nut, query_alias="cha_nut"),
                col(table.pst, query_alias="cha_pst"),
                col(table.ls_lnk, query_alias="cha_ls_lnk"),
                col(table.aqu_lnk, query_alias="cha_aqu_lnk")]
        self.write_query(query, cols)

        """if db.Channel_cha.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.int_pad("CHA_NUMB"))
                file.write(utils.string_pad("CHA_NAME", direction="left"))
                file.write(utils.string_pad("CHA_INI"))
                file.write(utils.string_pad("CHA_HYD"))
                file.write(utils.string_pad("CHA_SED"))
                file.write(utils.string_pad("CHA_NUT"))
                file.write(utils.string_pad("CHA_PST"))
                file.write(utils.string_pad("CHA_LS_LNK"))
                file.write(utils.string_pad("CHA_AQU_LNK"))
                file.write("\n")

                for cha in db.Channel_cha.select().order_by(db.Channel_cha.id):
                    file.write(utils.int_pad(cha.id))
                    file.write(utils.string_pad(cha.name, direction="left"))
                    file.write(utils.key_name_pad(cha.init))
                    file.write(utils.key_name_pad(cha.hyd))
                    file.write(utils.key_name_pad(cha.sed))
                    file.write(utils.key_name_pad(cha.nut))
                    file.write(utils.key_name_pad(cha.pst))
                    file.write(utils.key_name_pad(cha.ls_lnk, text_if_null="0"))
                    file.write(utils.key_name_pad(cha.aqu_lnk, text_if_null="0"))
                    file.write("\n")"""
