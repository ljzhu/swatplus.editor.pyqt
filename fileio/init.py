from .base import BaseFileModel
import utils
import database.project.init as db


class Initial_plt(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Initial_plt
        order_by = db.Initial_plt.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.string_pad("PCOM_NAME", direction="left"))
                file.write(utils.num_pad("PLT_CNT"))
                file.write(utils.string_pad("PLT_NAME"))
                file.write(utils.num_pad("IGRO"))
                file.write(utils.num_pad("PHU_MAT"))
                file.write(utils.num_pad("LAI_INI"))
                file.write(utils.num_pad("BM_INI"))
                file.write(utils.num_pad("PHU_ACC_INI"))
                file.write(utils.num_pad("POP"))
                file.write(utils.num_pad("YRS_INI"))
                file.write(utils.num_pad("RSD_INI"))
                file.write("\n")

                for row in table.select().order_by(order_by):
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write(utils.num_pad(row.plants.count()))
                    file.write("\n")

                    for plant in row.plants:
                        file.write(utils.string_pad("", text_if_null=""))
                        file.write(utils.string_pad("", text_if_null=""))
                        file.write(utils.key_name_pad(plant.cpnm, direction="left"))
                        file.write(utils.num_pad(plant.igro))
                        file.write(utils.num_pad(plant.phu))
                        file.write(utils.num_pad(plant.lai))
                        file.write(utils.num_pad(plant.bioms))
                        file.write(utils.num_pad(plant.phuacc))
                        file.write(utils.num_pad(plant.pop))
                        file.write(utils.num_pad(plant.yrmat))
                        file.write(utils.num_pad(plant.rsdin))

                    file.write("\n")