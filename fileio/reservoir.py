from .base import BaseFileModel
import utils
import database.project.reservoir as db


class Reservoir_res(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Reservoir_res
        order_by = db.Reservoir_res.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.int_pad("RES_NUMB"))
                file.write(utils.string_pad("RES_NAME", direction="left"))
                file.write(utils.string_pad("RES_INI"))
                file.write(utils.string_pad("RES_HYD"))
                file.write(utils.int_pad("RELEASE"))
                file.write(utils.string_pad("RES_SED"))
                file.write(utils.string_pad("RES_NUT"))
                file.write(utils.string_pad("RES_PST"))
                file.write("\n")

                for row in table.select().order_by(order_by):
                    file.write(utils.int_pad(row.id))
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write(utils.key_name_pad(row.init))
                    file.write(utils.key_name_pad(row.hyd))
                    file.write(utils.int_pad(row.release))
                    file.write(utils.key_name_pad(row.sed))
                    file.write(utils.key_name_pad(row.nut))
                    file.write(utils.key_name_pad(row.pst))
                    file.write("\n")