from .base import BaseFileModel
import utils
import database.project.routing_unit as db

from peewee import *


class Rout_unit(BaseFileModel):
    def __init__(self, file_name, def_file_name, version=None):
        self.file_name = file_name
        self.def_file_name = def_file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Rout_unit_rtu
        order_by = db.Rout_unit_rtu.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                with open(self.def_file_name, 'w') as def_file:
                    file.write(self.get_meta_line())
                    file.write(utils.int_pad("SUB_NUMB"))
                    file.write(utils.string_pad("SUB_NAME"))
                    file.write(utils.string_pad("SUB_DEFINE"))
                    file.write(utils.string_pad("ELEM_DR"))
                    file.write(utils.string_pad("SUBTOPO"))
                    file.write(utils.string_pad("FIELD"))
                    file.write("\n")

                    def_file.write(self.get_meta_line(alt_file_name=self.def_file_name))
                    def_file.write(utils.int_pad("SUB_NUMB"))
                    def_file.write(utils.string_pad("SUB_NAME"))
                    def_file.write(utils.int_pad("ELEM_TOT"))
                    def_file.write(utils.int_pad("ELEM1"))
                    def_file.write(utils.int_pad("ELEM2"))
                    def_file.write("\n")

                    for row in table.select().order_by(order_by):
                        file.write(utils.int_pad(row.id))
                        file.write(utils.string_pad(row.name))
                        file.write(utils.string_pad(row.name))
                        file.write(utils.key_name_pad(row.dr))
                        file.write(utils.key_name_pad(row.topo_hyd))
                        file.write(utils.key_name_pad(row.field))
                        file.write("\n")

                        def_file.write(utils.int_pad(row.id))
                        def_file.write(utils.string_pad(row.name))
                        def_file.write(utils.int_pad(row.elements.count()))

                        ele = row.elements.select(fn.Min(db.Rout_unit_ele.id), fn.Max(db.Rout_unit_ele.id)).scalar(as_tuple=True)
                        def_file.write(utils.int_pad(ele[0]))
                        def_file.write(utils.int_pad(ele[1] * -1))
                        def_file.write("\n")


class Rout_unit_ele(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Rout_unit_ele
        order_by = db.Rout_unit_ele.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.int_pad("NUMB"))
                file.write(utils.string_pad("NAME", direction="left"))
                file.write(utils.code_pad("OBTYP"))
                file.write(utils.int_pad("OBTYPNO"))
                file.write(utils.code_pad("HTYP"))
                file.write(utils.num_pad("FRAC"))
                file.write(utils.string_pad("IDR"))
                file.write("\n")

                for row in table.select().order_by(order_by):
                    file.write(utils.int_pad(row.id))
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write(utils.code_pad(row.obtyp))
                    file.write(utils.int_pad(row.obtypno))
                    file.write(utils.code_pad(row.htyp))
                    file.write(utils.num_pad(row.frac))
                    file.write(utils.key_name_pad(row.idr, text_if_null="0"))
                    file.write("\n")
