import os.path

from .base import BaseFileModel
import utils
import database.project.connect as db


def write_header(file, elem_name, has_con_out):
    file.write(utils.int_pad("NUMB"))
    file.write(utils.string_pad("NAME", direction="left"))
    file.write(utils.num_pad("AREA"))
    file.write(utils.num_pad("LAT"))
    file.write(utils.num_pad("LONG"))
    file.write(utils.num_pad("ELEV"))
    file.write(utils.int_pad(elem_name))
    file.write(utils.int_pad("WST"))
    file.write(utils.int_pad("CONSTIT"))
    file.write(utils.int_pad("OVERFLOW"))
    file.write(utils.int_pad("RULE"))
    file.write(utils.int_pad("OUT_TOT"))

    if has_con_out:
        file.write(utils.code_pad("OBTYP"))
        file.write(utils.int_pad("OBTYPNO"))
        file.write(utils.code_pad("HYTYP"))
        file.write(utils.num_pad("FRAC"))

    file.write("\n")


def write_row(file, con, elem_id, con_outs):
    file.write(utils.int_pad(con.id))
    file.write(utils.string_pad(con.name, direction="left"))
    file.write(utils.num_pad(con.area_ha))
    file.write(utils.num_pad(con.lat))
    file.write(utils.num_pad(con.long))
    file.write(utils.num_pad(con.elev))
    file.write(utils.int_pad(elem_id))
    file.write(utils.int_pad(con.wst_id))
    file.write(utils.int_pad(con.constit_id))
    file.write(utils.int_pad(con.overflow))
    file.write(utils.int_pad(con.ruleset))
    file.write(utils.int_pad(con_outs.count()))

    for out in con_outs:
        file.write(utils.code_pad(out.obtyp_out))
        file.write(utils.int_pad(out.obtyno_out))
        file.write(utils.code_pad(out.hytyp_out))
        file.write(utils.num_pad(out.frac_out))

    file.write("\n")


class Hru_con(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Hru_con.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                write_header(file, "HRU", db.Hru_con_out.select().count() > 0)

                for con in db.Hru_con.select().order_by(db.Hru_con.id):
                    write_row(file, con, con.hru_id, con.hru_con_outs.order_by(db.Hru_con_out.order))


class Rout_unit_con(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Rout_unit_con.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                write_header(file, "RU", db.Rout_unit_con_out.select().count() > 0)

                for con in db.Rout_unit_con.select().order_by(db.Rout_unit_con.id):
                    write_row(file, con, con.ru_id, con.rout_unit_con_outs.order_by(db.Rout_unit_con_out.order))


class Aquifer_con(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Aquifer_con.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                write_header(file, "AQU", db.Aquifer_con_out.select().count() > 0)

                for con in db.Aquifer_con.select().order_by(db.Aquifer_con.id):
                    write_row(file, con, con.aqu_id, con.aquifer_con_outs.order_by(db.Aquifer_con_out.order))


class Channel_con(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Channel_con.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                write_header(file, "CHA", db.Channel_con_out.select().count() > 0)

                for con in db.Channel_con.select().order_by(db.Channel_con.id):
                    write_row(file, con, con.cha_id, con.channel_con_outs.order_by(db.Channel_con_out.order))


class Reservoir_con(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Reservoir_con.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                write_header(file, "RES", db.Reservoir_con_out.select().count() > 0)

                for con in db.Reservoir_con.select().order_by(db.Reservoir_con.id):
                    write_row(file, con, con.res_id, con.reservoir_con_outs.order_by(db.Reservoir_con_out.order))


class Recall_con(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Recall_con.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                write_header(file, "REC", db.Recall_con_out.select().count() > 0)

                for con in db.Recall_con.select().order_by(db.Recall_con.id):
                    write_row(file, con, con.rec_id, con.recall_con_outs.order_by(db.Recall_con_out.order))


class Recall_rec(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Recall_rec
        order_by = db.Recall_rec.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.int_pad("REC_NUMB"))
                file.write(utils.string_pad("REC_NAME", direction="left"))
                file.write(utils.int_pad("REC_TYP"))
                file.write(utils.string_pad("FILENAME"))
                file.write("\n")

                for row in table.select().order_by(order_by):
                    file.write(utils.int_pad(row.id))
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write(utils.int_pad(row.type))
                    file.write(utils.string_pad(row.filename))
                    file.write("\n")
