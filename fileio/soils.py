from .base import BaseFileModel, FileColumn as col
from peewee import *
import utils
import database.project.soils as db


class Nutrients_sol(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        self.write_default_table(db.Nutrients_sol)


class Soils_sol(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        soils = db.Soils_sol.select().order_by(db.Soils_sol.id)
        layers = db.Soils_sol_layer.select().order_by(db.Soils_sol_layer.layer_num)
        query = prefetch(soils, layers)

        if soils.count() > 0:
            with open(self.file_name, 'w') as file:
                self.write_meta_line(file)
                header_cols = [col(db.Soils_sol.name, direction="left", padding_override=25),
                               col("LAY_CNT", not_in_db=True, padding_override=utils.DEFAULT_INT_PAD),
                               col(db.Soils_sol.hyd_grp),
                               col(db.Soils_sol.dp_tot),
                               col(db.Soils_sol.anion_excl),
                               col(db.Soils_sol.perc_crk),
                               col(db.Soils_sol.texture, direction="left", padding_override=25)]
                self.write_headers(file, header_cols)

                lt = db.Soils_sol_layer
                for i in range(1, 11):
                    layer_cols = [col(lt.dp, repeat=i),
                                  col(lt.bd, repeat=i),
                                  col(lt.awc, repeat=i),
                                  col(lt.soil_k, repeat=i),
                                  col(lt.carbon, repeat=i),
                                  col(lt.clay, repeat=i),
                                  col(lt.silt, repeat=i),
                                  col(lt.sand, repeat=i),
                                  col(lt.rock, repeat=i),
                                  col(lt.alb, repeat=i),
                                  col(lt.usle_k, repeat=i),
                                  col(lt.ec, repeat=i),
                                  col(lt.caco3, repeat=i),
                                  col(lt.ph, repeat=i)]
                    self.write_headers(file, layer_cols)

                file.write("\n")

                for row in query:
                    row_cols = [col(row.name, direction="left", padding_override=25),
                                col(row.layers.count()),
                                col(row.hydgrp),
                                col(row.zmx),
                                col(row.anion_excl),
                                col(row.crk),
                                col(row.texture, direction="left", padding_override=25)]
                    self.write_row(file, row_cols)

                    for layer in row.layers_prefetch:
                        layer_row_cols = [col(layer.z),
                                          col(layer.bd),
                                          col(layer.awc),
                                          col(layer.k),
                                          col(layer.cbn),
                                          col(layer.clay),
                                          col(layer.silt),
                                          col(layer.sand),
                                          col(layer.rock),
                                          col(layer.alb),
                                          col(layer.usle_k),
                                          col(layer.ec),
                                          col(layer.cal),
                                          col(layer.ph)]
                        self.write_row(file, layer_row_cols)

                    file.write("\n")