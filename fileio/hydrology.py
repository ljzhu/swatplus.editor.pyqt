from .base import BaseFileModel
import utils
import database.project.hydrology as db


class Topography_hyd(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Topography_hyd.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.string_pad("NAME", direction="left"))
                file.write(utils.num_pad("SLP"))
                file.write(utils.num_pad("SLP_LEN"))
                file.write(utils.num_pad("LAT_LEN"))
                file.write(utils.num_pad("DIS_STREAM"))
                file.write(utils.num_pad("DEP_CO"))
                file.write("\n")

                self.write_rows(file, db.Topography_hyd.select().where(db.Topography_hyd.type == "hru"))
                self.write_rows(file, db.Topography_hyd.select().where(db.Topography_hyd.type == "sub"))

    def write_rows(self, file, items):
        for row in items.order_by(db.Topography_hyd.id):
            file.write(utils.string_pad(row.name, direction="left"))
            file.write(utils.num_pad(row.slope))
            file.write(utils.num_pad(row.slope_len))
            file.write(utils.num_pad(row.lat_len))
            file.write(utils.num_pad(row.dis_stream))
            file.write(utils.num_pad(row.dep_co))
            file.write("\n")


class Hydrology_hyd(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        if db.Hydrology_hyd.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.string_pad("HYDRO_NAME", direction="left"))
                file.write(utils.num_pad("LAT_TTIME"))
                file.write(utils.num_pad("LAT_SED"))
                file.write(utils.num_pad("CAN_MAX"))
                file.write(utils.num_pad("ESCO"))
                file.write(utils.num_pad("EPCO"))
                file.write(utils.num_pad("ER_ORGN"))
                file.write(utils.num_pad("ER_ORGP"))
                file.write(utils.num_pad("EV_POT"))
                file.write(utils.num_pad("BIOMIX"))
                file.write(utils.num_pad("DP_IMP"))
                file.write(utils.num_pad("LAT_ORGN"))
                file.write(utils.num_pad("LAT_ORGP"))
                file.write(utils.num_pad("HARG_PET"))
                file.write(utils.num_pad("CN_CO"))
                file.write("\n")

                for row in db.Hydrology_hyd.select().order_by(db.Hydrology_hyd.id):
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write(utils.num_pad(row.lat_ttime))
                    file.write(utils.num_pad(row.lat_sed))
                    file.write(utils.num_pad(row.canmx))
                    file.write(utils.num_pad(row.esco))
                    file.write(utils.num_pad(row.epco))
                    file.write(utils.num_pad(row.erorgn))
                    file.write(utils.num_pad(row.erorgp))
                    file.write(utils.num_pad(row.cn3_swf))
                    file.write(utils.num_pad(row.biomix))
                    file.write(utils.num_pad(row.dep_imp))
                    file.write(utils.num_pad(row.lat_orgn))
                    file.write(utils.num_pad(row.lat_orgp))
                    file.write(utils.num_pad(row.harg_pet))
                    file.write(utils.num_pad(row.cncoef))
                    file.write("\n")