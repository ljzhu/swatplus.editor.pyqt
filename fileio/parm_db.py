from .base import BaseFileModel
from database.project import base as project_base
from database.project import parm_db as project_parmdb
from database.datasets import base as datasets_base
from database.datasets import parm_db as datasets_parmdb
from database import lib as db_lib

import utils
import database.project.parm_db as db


class Plants_plt(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self, database ='project'):
        """
        Read a plants.plt text file into the database.
        :param database: project or datasets
        :return: 
        """
        file = open(self.file_name, "r")

        i = 1
        plants = []
        for line in file:
            if i > 2:
                val = line.split()
                self.check_cols(val, 48, 'plants')

                plant = {
                    'name': val[0].lower(),
                    'plnt_typ': val[1],
                    'plnt_hu': val[2],
                    'bm_e': val[3],
                    'harv_idx': val[4],
                    'lai_pot': val[5],
                    'frac_hu1': val[6],
                    'lai_max1': val[7],
                    'frac_hu2': val[8],
                    'lai_max2': val[9],
                    'hu_lai_decl': val[10],
                    'can_ht_max': val[11],
                    'rt_dp_max': val[12],
                    'tmp_opt': val[13],
                    'tmp_base': val[14],
                    'frac_n_yld': val[15],
                    'frac_p_yld': val[16],
                    'frac_n_em': val[17],
                    'frac_n_50': val[18],
                    'frac_n_mat': val[19],
                    'frac_p_em': val[20],
                    'frac_p_50': val[21],
                    'frac_p_mat': val[22],
                    'harv_idx_ws': val[23],
                    'usle_c_min': val[24],
                    'stcon_max': val[25],
                    'vpd': val[26],
                    'frac_stcon': val[27],
                    'ru_vpd': val[28],
                    'co2_hi': val[29],
                    'bm_e_hi': val[30],
                    'plnt_decomp': val[31],
                    'lai_min': val[32],
                    'bm_tree_acc': val[33],
                    'yrs_mat': val[34],
                    'bm_tree_max': val[35],
                    'ext_co': val[36],
                    'bm_dieoff': val[37],
                    'rt_st_beg': val[38],
                    'rt_st_end': val[39],
                    'plnt_pop1': val[40],
                    'frac_lai1': val[41],
                    'plnt_pop2': val[42],
                    'frac_lai2': val[43],
                    'frac_sw_gro': val[44],
                    'wnd_live': val[45],
                    'wnd_dead': val[46],
                    'wnd_flat': val[47],
                    'description': val[48] if val[48] != 'null' else None
                }
                plants.append(plant)
            i += 1

        if database == 'project':
            db_lib.bulk_insert(project_base.db, project_parmdb.Plants_plt, plants)
        else:
            db_lib.bulk_insert(datasets_base.db, datasets_parmdb.Plants_plt, plants)

    def write(self):
        table = db.Plants_plt
        order_by = db.Plants_plt.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.string_pad("NAME", direction="left"))
                file.write("\n")

                for row in table.select().order_by(order_by):
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write("\n")


class Fertilizer_frt(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self, database ='project'):
        """
        Read a fertilizer.frt text file into the database.
        :param database: project or datasets
        :return: 
        """
        file = open(self.file_name, "r")

        i = 1
        ferts = []
        for line in file:
            if i > 2:
                val = line.split()
                self.check_cols(val, 10, 'fertilizer')

                fert = {
                    'name': val[0].lower(),
                    'min_n': val[1],
                    'min_p': val[2],
                    'org_n': val[3],
                    'org_p': val[4],
                    'nh3_n': val[5],
                    'p_bact': val[6],
                    'lp_bact': val[7],
                    'sol_bact': val[8],
                    'description': val[9] if val[9] != 'null' else None
                }
                ferts.append(fert)
            i += 1

        if database == 'project':
            db_lib.bulk_insert(project_base.db, project_parmdb.Fertilizer_frt, ferts)
        else:
            db_lib.bulk_insert(datasets_base.db, datasets_parmdb.Fertilizer_frt, ferts)

    def write(self):
        raise NotImplementedError('Writing fertilizer.frt from database not implemented yet.')


class Tillage_til(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self, database ='project'):
        """
        Read a tillage.til text file into the database.
        :param database: project or datasets
        :return: 
        """
        file = open(self.file_name, "r")

        i = 1
        tils = []
        for line in file:
            if i > 2:
                val = line.split()
                self.check_cols(val, 7, 'tillage')

                til = {
                    'name': val[0].lower(),
                    'mix_eff': val[1],
                    'mix_dp': val[2],
                    'rough': val[3],
                    'ridge_ht': val[4],
                    'ridge_sp': val[5],
                    'description': val[6] if val[6] != 'null' else None
                }
                tils.append(til)
            i += 1

        if database == 'project':
            db_lib.bulk_insert(project_base.db, project_parmdb.Tillage_til, tils)
        else:
            db_lib.bulk_insert(datasets_base.db, datasets_parmdb.Tillage_til, tils)

    def write(self):
        raise NotImplementedError('Writing tillage.til from database not implemented yet.')


class Pesticide_pst(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self, database ='project'):
        """
        Read a pesticide.pst text file into the database.
        :param database: project or datasets
        :return: 
        """
        file = open(self.file_name, "r")

        i = 1
        pests = []
        for line in file:
            if i > 2:
                val = line.split()
                self.check_cols(val, 8, 'pesticides')

                pest = {
                    'name': val[0].lower(),
                    'soil_ads': val[1],
                    'frac_wash': val[2],
                    'hl_foliage': val[3],
                    'hl_soil': val[4],
                    'app_eff': val[5],
                    'pest_solub': val[6],
                    'description': val[7] if val[7] != 'null' else None
                }
                pests.append(pest)
            i += 1

        if database == 'project':
            db_lib.bulk_insert(project_base.db, project_parmdb.Pesticide_pst, pests)
        else:
            db_lib.bulk_insert(datasets_base.db, datasets_parmdb.Pesticide_pst, pests)

    def write(self):
        raise NotImplementedError('Writing pesticide.pst from database not implemented yet.')


class Urban_urb(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self, database ='project'):
        """
        Read a urban.urb text file into the database.
        :param database: project or datasets
        :return: 
        """
        file = open(self.file_name, "r")

        i = 1
        urbans = []
        for line in file:
            if i > 2:
                val = line.split()
                self.check_cols(val, 12, 'urban')

                urban = {
                    'name': val[0].lower(),
                    'frac_imp': val[1],
                    'frac_dc_imp': val[2],
                    'curb_den': val[3],
                    'urb_wash': val[4],
                    'dirt_max': val[5],
                    't_halfmax': val[6],
                    'conc_totn': val[7],
                    'conc_totp': val[8],
                    'conc_no3n': val[9],
                    'urb_cn': val[10],
                    'description': val[11] if val[11] != 'null' else None
                }
                urbans.append(urban)
            i += 1

        if database == 'project':
            db_lib.bulk_insert(project_base.db, project_parmdb.Urban_urb, urbans)
        else:
            db_lib.bulk_insert(datasets_base.db, datasets_parmdb.Urban_urb, urbans)

    def write(self):
        raise NotImplementedError('Writing urban.urb from database not implemented yet.')


class Septic_sep(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self, database ='project'):
        """
        Read a septic.sep text file into the database.
        NOTE: CURRENTLY THERE IS AN EXTRA NUMERIC COLUMN BEFORE THE DESCRIPTION.
        :param database: project or datasets
        :return: 
        """
        file = open(self.file_name, "r")

        i = 1
        septics = []
        for line in file:
            if i > 2:
                val = line.split()
                self.check_cols(val, 13, 'septic')

                sep = {
                    'name': val[0].lower(),
                    'q_rate': val[1],
                    'bod': val[2],
                    'tss': val[3],
                    'nh4_n': val[4],
                    'no3_n': val[5],
                    'no2_n': val[6],
                    'org_n': val[7],
                    'min_p': val[8],
                    'org_p': val[9],
                    'fcoli': val[10],
                    'description': val[12] if val[12] != 'null' else None  # 12 index because extra column
                }
                septics.append(sep)
            i += 1

        if database == 'project':
            db_lib.bulk_insert(project_base.db, project_parmdb.Septic_sep, septics)
        else:
            db_lib.bulk_insert(datasets_base.db, datasets_parmdb.Septic_sep, septics)

    def write(self):
        raise NotImplementedError('Writing septic.sep from database not implemented yet.')
