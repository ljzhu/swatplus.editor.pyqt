from .base import BaseFileModel
import utils
import database.project.lum as db


class Landuse_lum(BaseFileModel):
    def __init__(self, file_name, version=None):
        self.file_name = file_name
        self.version = version

    def read(self):
        raise NotImplementedError('Reading not implemented yet.')

    def write(self):
        table = db.Landuse_lum
        order_by = db.Landuse_lum.id

        if table.select().count() > 0:
            with open(self.file_name, 'w') as file:
                file.write(self.get_meta_line())
                file.write(utils.string_pad("LULC_NAME", direction="left"))
                file.write(utils.string_pad("PCOM_NAME"))
                file.write(utils.string_pad("MGT_NAME"))
                file.write(utils.string_pad("CN2"))
                file.write(utils.string_pad("CONS_PRAC"))
                file.write(utils.string_pad("URB_NAME"))
                file.write(utils.string_pad("URB_FLAG"))
                file.write(utils.string_pad("OV_MANN"))
                file.write(utils.string_pad("TILE"))
                file.write(utils.string_pad("SEPTIC"))
                file.write(utils.string_pad("VFS"))
                file.write(utils.string_pad("GRWW"))
                file.write(utils.string_pad("BMP"))
                file.write("\n")

                for row in table.select().order_by(order_by):
                    file.write(utils.string_pad(row.name, direction="left"))
                    file.write(utils.key_name_pad(row.plant_cov))
                    file.write(utils.key_name_pad(row.mgt_ops))
                    file.write(utils.key_name_pad(row.cn_lu))
                    file.write(utils.key_name_pad(row.cons_prac))
                    file.write(utils.key_name_pad(row.urb_lu))
                    file.write(utils.string_pad(row.urb_ro))
                    file.write(utils.key_name_pad(row.ovn))
                    file.write(utils.key_name_pad(row.tiledrain))
                    file.write(utils.key_name_pad(row.septic))
                    file.write(utils.key_name_pad(row.fstrip))
                    file.write(utils.key_name_pad(row.grassww))
                    file.write(utils.key_name_pad(row.bmpuser))
                    file.write("\n")