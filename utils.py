import re

DEFAULT_STR_PAD = 16
DEFAULT_KEY_PAD = 16
DEFAULT_CODE_PAD = 12
DEFAULT_NUM_PAD = 12
DEFAULT_INT_PAD = 8
DEFAULT_DECIMALS = 3
DEFAULT_DIRECTION = "right"
DEFAULT_SPACES_AFTER = 2
NULL_STR = "null"
NULL_NUM = "0"


def get_valid_filename(s):
    s = s.strip().replace(' ', '_')
    return re.sub(r'(?u)[^-\w.]', '', s)


def remove_space(s, c='_'):
    return s.strip().replace(' ', c)


def string_pad(val, default_pad=DEFAULT_STR_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_STR, spaces_after=DEFAULT_SPACES_AFTER):
    val_text = text_if_null if val is None else remove_space(val)

    space = ""
    for x in range(0, spaces_after):
        space += " "

    if direction == "right":
        return str(val_text).rjust(default_pad) + space
    else:
        return str(val_text).ljust(default_pad) + space


def code_pad(val, default_pad=DEFAULT_CODE_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_STR):
    return string_pad(val, default_pad, direction, text_if_null)


def key_name_pad(prop, default_pad=DEFAULT_KEY_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_STR):
    val = None if prop is None else prop.name
    return string_pad(val, default_pad, direction, text_if_null)


def num_pad(val, decimals=DEFAULT_DECIMALS, default_pad=DEFAULT_NUM_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_NUM):
    val_text = val
    if is_number(val):
        val_text = "{:.{prec}f}".format(val, prec=decimals)

    return string_pad(val_text, default_pad, direction, text_if_null)


def int_pad(val, default_pad=DEFAULT_INT_PAD, direction=DEFAULT_DIRECTION):
    return num_pad(val, 0, default_pad, direction, NULL_NUM)


def write_string(file, val, default_pad=DEFAULT_STR_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_STR, spaces_after=DEFAULT_SPACES_AFTER):
    file.write(string_pad(val, default_pad, direction, text_if_null, spaces_after))


def write_code(file, val, default_pad=DEFAULT_CODE_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_STR):
    file.write(code_pad(val, default_pad, direction, text_if_null))


def write_key_name(file, prop, default_pad=DEFAULT_KEY_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_STR):
    file.write(key_name_pad(prop, default_pad, direction, text_if_null))


def write_num(file, val, decimals=DEFAULT_DECIMALS, default_pad=DEFAULT_NUM_PAD, direction=DEFAULT_DIRECTION, text_if_null=NULL_NUM):
    file.write(num_pad(val, decimals, default_pad, direction, text_if_null))


def write_int(file, val, default_pad=DEFAULT_INT_PAD, direction=DEFAULT_DIRECTION):
    file.write(int_pad(val, default_pad, direction))


def write_desc_string(file, val):
    if val is not None:
        file.write(val)


def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False
    except TypeError:
        return False
