from peewee import *
from . import base, definitions, parm_db
import fileio.parm_db
files_parmdb = fileio.parm_db


class SetupDatasetsDatabase():
    @staticmethod
    def create_tables():
        base.db.create_tables([definitions.Var_code, definitions.Var_range, definitions.File_cio_classification, definitions.File_cio], True)
        base.db.create_tables(
            [parm_db.Plants_plt, parm_db.Fertilizer_frt, parm_db.Tillage_til, parm_db.Pesticide_pst, parm_db.Urban_urb,
             parm_db.Septic_sep, parm_db.Snow_sno, parm_db.Soil, parm_db.Soil_layer], True)

    @staticmethod
    def initialize_data():
        codes = [
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'hru', 'description': 'hru'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'hlt', 'description': 'hru_lte'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'sub', 'description': 'subbasin'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'mfl', 'description': 'modflow'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'aqu', 'description': 'aquifer'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'cha', 'description': 'channel'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'res', 'description': 'reservoir'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'exc', 'description': 'export coefficients'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'del', 'description': 'delivery ratio'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'out', 'description': 'outlet'},
            {'table': 'connect', 'variable': 'obj_typ', 'code': 'sdc', 'description': 'swat-deg channel'},
            {'table': 'connect', 'variable': 'hyd_typ', 'code': 'tot', 'description': 'total flow'},
            {'table': 'connect', 'variable': 'hyd_typ', 'code': 'rhg', 'description': 'recharge'},
            {'table': 'connect', 'variable': 'hyd_typ', 'code': 'sur', 'description': 'surface'},
            {'table': 'connect', 'variable': 'hyd_typ', 'code': 'lat', 'description': 'lateral'},
            {'table': 'connect', 'variable': 'hyd_typ', 'code': 'til', 'description': 'tile'}
        ]

        with base.db.atomic():
            if definitions.Var_code.select().count() < 1:
                definitions.Var_code.insert_many(codes).execute()

        if parm_db.Plants_plt.select().count() < 1:
            files_parmdb.Plants_plt('data/source-data/parm_db/plants.plt').read('datasets')

        if parm_db.Fertilizer_frt.select().count() < 1:
            files_parmdb.Fertilizer_frt('data/source-data/parm_db/fertilizer.frt').read('datasets')

        if parm_db.Tillage_til.select().count() < 1:
            files_parmdb.Tillage_til('data/source-data/parm_db/tillage.til').read('datasets')

        if parm_db.Pesticide_pst.select().count() < 1:
            files_parmdb.Pesticide_pst('data/source-data/parm_db/pesticide.pst').read('datasets')

        if parm_db.Urban_urb.select().count() < 1:
            files_parmdb.Urban_urb('data/source-data/parm_db/urban.urb').read('datasets')

        if parm_db.Septic_sep.select().count() < 1:
            files_parmdb.Septic_sep('data/source-data/parm_db/septic.sep').read('datasets')

        """file_cio = [
            {
                'section': 'simulation',
                'default_file_names': 'time.sim,print.prt,object.prt,object.cnt',
                'txt_file_names': 'time_sim.txt,print.txt,object_prt.txt,object_cnt.txt'
            },
            {
                'section': 'climate',
                'default_file_names': 'weather-sta.cli,weather-wgn.cli,wind-dir.cli,pcp.cli,tmp.cli,slr.cli,hmd.cli,wnd.cli',
                'txt_file_names': 'cli_weather_sta.txt,cli_weather_wgn.txt,cli_wind_dir.txt,cli_pcp.txt,cli_tmp.txt,cli_slr.txt,cli_hmd.txt,cli_wnd.txt'
            },
            {
                'section': 'connect',
                'default_file_names': 'hru.con,hru-lte.con,rout_unit.con,modflow.con,aquifer.con,aquifer2d.con,channel.con,reservoir.con,recall.con,exco.con,delratio.con,outlet.con,chandeg.con',
                'txt_file_names': 'con_hru.txt,con_hru_lte.txt,con_rout_unit.txt,con_modflow.txt,con_aquifer.txt,con_aquifer2d.txt,con_channel.txt,con_reservoir.txt,con_recall.txt,con_exco.txt,con_delratio.txt,con_outlet.txt,con_chandeg.txt'
            },
            {
                'section': 'channel',
                'default_file_names': 'initial.cha,channel.cha,hydrology.cha,sediment.cha,nutrients.cha,pesticide.cha,channel-lte.cha',
                'txt_file_names': 'cha_initial.txt,channel.txt,cha_hydrology.txt,cha_sediment.txt,cha_nutrients.txt,cha_pesticide.txt,channel_lte.txt'},
            {
                'section': 'reservoir',
                'default_file_names': 'initial.res,reservoir.res,hydrology.res,nutrients.res,pesticide.res,sediment.res,weir.res',
                'txt_file_names': 'res_initial.txt,reservoir.txt,res_hydrology.txt,res_nutrients.txt,res_pesticide.txt,res_sediment.txt,res_weir.txt'
            },
            {
                'section': 'routing_unit',
                'default_file_names': 'rout_unit.def,rout_unit.ele,rout_unit.ru,rout_unit.dr',
                'txt_file_names': 'rout_unit_def.txt,rout_unit_ele.txt,rout_unit.txt,rout_unit_dr.txt'
            },
            {
                'section': 'hru',
                'default_file_names': 'hru-data.hru,hru-lte.hru',
                'txt_file_names': 'hru_data.txt,hru_lte.txt'
            },
            {
                'section': 'dr',
                'default_file_names': 'delratio.del',
                'txt_file_names': 'delratio.txt'
            },
            {
                'section': 'aquifer',
                'default_file_names': 'aquifer.aqu',
                'txt_file_names': 'aquifer.txt'
            },
            {
                'section': 'herd',
                'default_file_names': 'animal.hrd,herd.hrd,ranch.hrd',
                'txt_file_names': 'herd_animal.txt,herd.txt,herd_ranch.txt'
            },
            {
                'section': 'water_rights',
                'default_file_names': 'define.wro,element.wro,water_rights.wro',
                'txt_file_names': 'wro_define.txt,wro_element.txt,wro.txt'
            },
            {
                'section': 'link',
                'default_file_names': 'chan-surf.lin,chan-aqu.lin',
                'txt_file_names': 'link_chan_surf.txt,link_chan_aqu.txt'
            },
            {
                'section': 'basin',
                'default_file_names': 'codes.bsn,parameters.bsn',
                'txt_file_names': 'bsn_codes.txt,bsn_parameters.txt'
            },
            {
                'section': 'hydrology',
                'default_file_names': 'hydrology.hyd,topography.hyd,field.fld',
                'txt_file_names': 'hydrology.txt,hyd_topography.txt,hyd_field.txt'
            },
            {
                'section': 'exco',
                'default_file_names': 'exco.exc',
                'txt_file_names': 'exco.txt'
            },
            {
                'section': 'bacteria',
                'default_file_names': 'initial.bac,bacteria.bac',
                'txt_file_names': 'bac_initial.txt,bacteria.txt'
            },
            {
                'section': 'structural',
                'default_file_names': 'tiledrain.str,septic.str,filterstrip.str,grassedww.str,bmpuser.str',
                'txt_file_names': 'str_tiledrain.txt,str_septic.txt,str_filterstrip.txt,str_grassedww.txt,str_bmpuser.txt'
            },
            {
                'section': 'parm_db',
                'default_file_names': 'plants.plt,fertilizer.frt,tillage.til,pesticide.pst,urban.urb,septic.sep,snow.sno,atmo.atm',
                'txt_file_names': 'db_plants.txt,db_fertilizer.txt,db_tillage.txt,db_pesticide.txt,db_urban.txt,db_septic.txt,db_snow.txt,db_atmo.txt'
            },
            {
                'section': 'ops',
                'default_file_names': 'harv.ops,graze.ops,irr.ops,chem_app.ops,fire.ops,sweep.ops',
                'txt_file_names': 'ops_harv.txt,ops_graze.txt,ops_irr.txt,ops_chem_app.txt,ops_fire.txt,ops_sweep.txt'
            },
            {
                'section': 'lum',
                'default_file_names': 'landuse.lum,management.sch,cntable.lum,cons_prac.lum,ovn_table.lum',
                'txt_file_names': 'lum.txt,sch_management.txt,lum_cntable.txt,lum_cons_prac.txt,lum_ovn_table.txt'
            },
            {
                'section': 'chg',
                'default_file_names': 'codes.cal,cal_parms.cal,calibration.cal,ls_parms.cal,ls_regions.cal,ls_regions_lte.cal,ch_orders.cal',
                'txt_file_names': 'cal_codes.txt,cal_parms.txt,calibration.txt,cal_ls_parms.txt,cal_ls_regions.txt,cal_ls_regions_lte.txt,cal_ch_orders.txt'
            },
            {
                'section': 'init',
                'default_file_names': 'initial.pst,initial.plt',
                'txt_file_names': 'pst_initial.txt,plt_initial.txt'
            },
            {
                'section': 'soils',
                'default_file_names': 'soils.sol,nutrients.sol',
                'txt_file_names': 'soils.txt,soils_nutrients.txt'
            },
            {
                'section': 'decision_table',
                'default_file_names': 'dtable.dtl',
                'txt_file_names': 'dtable.txt'
            },
            {
                'section': 'constituents',
                'default_file_names': 'constituents.cs,pest.cs,path.cs,hmet.cs,salt.cs',
                'txt_file_names': 'constituents.txt,cs_pest.txt,cs_path.txt,cs_hmet.txt,cs_salt.txt'
            },
            {
                'section': 'regions',
                'default_file_names': 'ls_unit.ele,ls_unit.def,ls_reg.def,ls_cal.reg,ch_catunit.ele,ch_catunit.def,ch_reg.def,aqu_catunit.ele,aqu_catunit.def,aqu_reg.def,res_catunit.ele,res_catunit.def,res_reg.def,rec_catunit.ele,rec_catunit.def,rec_reg.def',
                'txt_file_names': 'ls_unit_ele.txt,ls_unit_def.txt,ls_reg_def.txt,ls_cal_reg.txt,ch_catunit_ele.txt,ch_catunit_def.txt,ch_reg_def.txt,aqu_catunit_ele.txt,aqu_catunit_def.txt,aqu_reg_def.txt,res_catunit_ele.txt,res_catunit_def.txt,res_reg_def.txt,rec_catunit_ele.txt,rec_catunit_def.txt,rec_reg_def.txt'
            },
        ]"""

        classifications = [
            {'id': 1, 'name': 'simulation'},
            {'id': 2, 'name': 'climate'},
            {'id': 3, 'name': 'connect'},
            {'id': 4, 'name': 'channel'},
            {'id': 5, 'name': 'reservoir'},
            {'id': 6, 'name': 'routing_unit'},
            {'id': 7, 'name': 'hru'},
            {'id': 8, 'name': 'dr'},
            {'id': 9, 'name': 'aquifer'},
            {'id': 10, 'name': 'herd'},
            {'id': 11, 'name': 'water_rights'},
            {'id': 12, 'name': 'link'},
            {'id': 13, 'name': 'basin'},
            {'id': 14, 'name': 'hydrology'},
            {'id': 15, 'name': 'exco'},
            {'id': 16, 'name': 'bacteria'},
            {'id': 17, 'name': 'structural'},
            {'id': 18, 'name': 'parm_db'},
            {'id': 19, 'name': 'ops'},
            {'id': 20, 'name': 'lum'},
            {'id': 21, 'name': 'chg'},
            {'id': 22, 'name': 'init'},
            {'id': 23, 'name': 'soils'},
            {'id': 24, 'name': 'decision_table'},
            {'id': 25, 'name': 'constituents'},
            {'id': 26, 'name': 'regions'},
            {'id': 27, 'name': 'pcp_path'},
            {'id': 28, 'name': 'tmp_path'},
            {'id': 29, 'name': 'slr_path'},
            {'id': 30, 'name': 'hmd_path'},
            {'id': 31, 'name': 'wnd_path'}
        ]

        file_cio = [
            {'classification': 1, 'order_in_class': 1, 'default_file_name': 'time.sim', 'database_table': 'time_sim', 'is_core_file': True},
            {'classification': 1, 'order_in_class': 2, 'default_file_name': 'print.prt', 'database_table': 'print_prt', 'is_core_file': True},
            {'classification': 1, 'order_in_class': 3, 'default_file_name': 'object.prt', 'database_table': 'object_prt', 'is_core_file': False},
            {'classification': 1, 'order_in_class': 4, 'default_file_name': 'object.cnt', 'database_table': 'object_cnt', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 1, 'default_file_name': 'weather-sta.cli', 'database_table': 'weather_sta_cli', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 2, 'default_file_name': 'weather-wgn.cli', 'database_table': 'weather_wgn_cli', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 3, 'default_file_name': 'wind-dir.cli', 'database_table': 'wind_dir_cli', 'is_core_file': False},
            {'classification': 2, 'order_in_class': 4, 'default_file_name': 'pcp.cli', 'database_table': 'weather_file', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 5, 'default_file_name': 'tmp.cli', 'database_table': 'weather_file', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 6, 'default_file_name': 'slr.cli', 'database_table': 'weather_file', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 7, 'default_file_name': 'hmd.cli', 'database_table': 'weather_file', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 8, 'default_file_name': 'wnd.cli', 'database_table': 'weather_file', 'is_core_file': True},
            {'classification': 2, 'order_in_class': 9, 'default_file_name': 'atmodep.cli', 'database_table': 'atmodep_cli', 'is_core_file': False},
            {'classification': 3, 'order_in_class': 1, 'default_file_name': 'hru.con', 'database_table': 'hru_con', 'is_core_file': True},
            {'classification': 3, 'order_in_class': 2, 'default_file_name': 'hru-lte.con', 'database_table': 'hru_lte_con', 'is_core_file': False},
            {'classification': 3, 'order_in_class': 3, 'default_file_name': 'rout_unit.con', 'database_table': 'rout_unit_con', 'is_core_file': True},
            {'classification': 3, 'order_in_class': 4, 'default_file_name': 'modflow.con', 'database_table': 'modflow_con', 'is_core_file': False},
            {'classification': 3, 'order_in_class': 5, 'default_file_name': 'aquifer.con', 'database_table': 'aquifer_con', 'is_core_file': True},
            {'classification': 3, 'order_in_class': 6, 'default_file_name': 'aquifer2d.con', 'database_table': 'aquifer2d_con', 'is_core_file': False},
            {'classification': 3, 'order_in_class': 7, 'default_file_name': 'channel.con', 'database_table': 'channel_con', 'is_core_file': True},
            {'classification': 3, 'order_in_class': 8, 'default_file_name': 'reservoir.con', 'database_table': 'reservoir_con', 'is_core_file': True},
            {'classification': 3, 'order_in_class': 9, 'default_file_name': 'recall.con', 'database_table': 'recall_con', 'is_core_file': True},
            {'classification': 3, 'order_in_class': 10, 'default_file_name': 'exco.con', 'database_table': 'exco_con', 'is_core_file': False},
            {'classification': 3, 'order_in_class': 11, 'default_file_name': 'delratio.con', 'database_table': 'delratio_con', 'is_core_file': False},
            {'classification': 3, 'order_in_class': 12, 'default_file_name': 'outlet.con', 'database_table': 'outlet_con', 'is_core_file': True},
            {'classification': 3, 'order_in_class': 13, 'default_file_name': 'chandeg.con', 'database_table': 'chandeg_con', 'is_core_file': False},
            {'classification': 4, 'order_in_class': 1, 'default_file_name': 'initial.cha', 'database_table': 'initial_cha', 'is_core_file': True},
            {'classification': 4, 'order_in_class': 2, 'default_file_name': 'channel.cha', 'database_table': 'channel_cha', 'is_core_file': True},
            {'classification': 4, 'order_in_class': 3, 'default_file_name': 'hydrology.cha', 'database_table': 'hydrology_cha', 'is_core_file': True},
            {'classification': 4, 'order_in_class': 4, 'default_file_name': 'sediment.cha', 'database_table': 'sediment_cha', 'is_core_file': True},
            {'classification': 4, 'order_in_class': 5, 'default_file_name': 'nutrients.cha', 'database_table': 'nutrients_cha', 'is_core_file': True},
            {'classification': 4, 'order_in_class': 6, 'default_file_name': 'pesticide.cha', 'database_table': 'pesticide_cha', 'is_core_file': False},
            {'classification': 4, 'order_in_class': 7, 'default_file_name': 'channel-lte.cha', 'database_table': 'channel_lte_cha', 'is_core_file': False},
            {'classification': 5, 'order_in_class': 1, 'default_file_name': 'initial.res', 'database_table': 'initial_res', 'is_core_file': True},
            {'classification': 5, 'order_in_class': 2, 'default_file_name': 'reservoir.res', 'database_table': 'reservoir_res', 'is_core_file': True},
            {'classification': 5, 'order_in_class': 3, 'default_file_name': 'hydrology.res', 'database_table': 'hydrology_res', 'is_core_file': True},
            {'classification': 5, 'order_in_class': 4, 'default_file_name': 'nutrients.res', 'database_table': 'nutrients_res', 'is_core_file': True},
            {'classification': 5, 'order_in_class': 5, 'default_file_name': 'pesticide.res', 'database_table': 'pesticide_res', 'is_core_file': False},
            {'classification': 5, 'order_in_class': 6, 'default_file_name': 'sediment.res', 'database_table': 'sediment_res', 'is_core_file': True},
            {'classification': 5, 'order_in_class': 7, 'default_file_name': 'weir.res', 'database_table': 'weir_res', 'is_core_file': False},
            {'classification': 5, 'order_in_class': 8, 'default_file_name': 'wetland.wet', 'database_table': 'wetland_wet', 'is_core_file': False},
            {'classification': 5, 'order_in_class': 9, 'default_file_name': 'hydrology.wet', 'database_table': 'hydrology_wet', 'is_core_file': False},
            {'classification': 6, 'order_in_class': 1, 'default_file_name': 'rout_unit.def', 'database_table': '', 'is_core_file': True},
            {'classification': 6, 'order_in_class': 2, 'default_file_name': 'rout_unit.ele', 'database_table': 'rout_unit_ele', 'is_core_file': True},
            {'classification': 6, 'order_in_class': 3, 'default_file_name': 'rout_unit.rtu', 'database_table': 'rout_unit_rtu', 'is_core_file': True},
            {'classification': 6, 'order_in_class': 4, 'default_file_name': 'rout_unit.dr', 'database_table': 'rout_unit_dr', 'is_core_file': False},
            {'classification': 7, 'order_in_class': 1, 'default_file_name': 'hru-data.hru', 'database_table': 'hru_data_hru', 'is_core_file': True},
            {'classification': 7, 'order_in_class': 2, 'default_file_name': 'hru-lte.hru', 'database_table': 'hru_lte_hru', 'is_core_file': False},
            {'classification': 8, 'order_in_class': 1, 'default_file_name': 'delratio.del', 'database_table': 'delratio_del', 'is_core_file': False},
            {'classification': 9, 'order_in_class': 1, 'default_file_name': 'aquifer.aqu', 'database_table': 'aquifer_aqu', 'is_core_file': True},
            {'classification': 10, 'order_in_class': 1, 'default_file_name': 'animal.hrd', 'database_table': 'animal_hrd', 'is_core_file': False},
            {'classification': 10, 'order_in_class': 2, 'default_file_name': 'herd.hrd', 'database_table': 'herd_hrd', 'is_core_file': False},
            {'classification': 10, 'order_in_class': 3, 'default_file_name': 'ranch.hrd', 'database_table': 'ranch_hrd', 'is_core_file': False},
            {'classification': 11, 'order_in_class': 1, 'default_file_name': 'define.wro', 'database_table': 'define_wro', 'is_core_file': False},
            {'classification': 11, 'order_in_class': 2, 'default_file_name': 'element.wro', 'database_table': 'element_wro', 'is_core_file': False},
            {'classification': 11, 'order_in_class': 3, 'default_file_name': 'water_rights.wro', 'database_table': 'water_rights_wro', 'is_core_file': False},
            {'classification': 12, 'order_in_class': 1, 'default_file_name': 'chan-surf.lin', 'database_table': 'chan_surf_lin', 'is_core_file': False},
            {'classification': 12, 'order_in_class': 2, 'default_file_name': 'chan-aqu.lin', 'database_table': 'chan_aqu_lin', 'is_core_file': False},
            {'classification': 13, 'order_in_class': 1, 'default_file_name': 'codes.bsn', 'database_table': 'codes_bsn', 'is_core_file': True},
            {'classification': 13, 'order_in_class': 2, 'default_file_name': 'parameters.bsn', 'database_table': 'parameters_bsn', 'is_core_file': True},
            {'classification': 14, 'order_in_class': 1, 'default_file_name': 'hydrology.hyd', 'database_table': 'hydrology_hyd', 'is_core_file': True},
            {'classification': 14, 'order_in_class': 2, 'default_file_name': 'topography.hyd', 'database_table': 'topography_hyd', 'is_core_file': True},
            {'classification': 14, 'order_in_class': 3, 'default_file_name': 'field.fld', 'database_table': 'field_fld', 'is_core_file': True},
            {'classification': 15, 'order_in_class': 1, 'default_file_name': 'exco.exc', 'database_table': 'exco_exc', 'is_core_file': False},
            {'classification': 15, 'order_in_class': 2, 'default_file_name': 'recall.rec', 'database_table': 'recall_rec', 'is_core_file': True},
            {'classification': 16, 'order_in_class': 1, 'default_file_name': 'initial.bac', 'database_table': 'initial_bac', 'is_core_file': False},
            {'classification': 16, 'order_in_class': 2, 'default_file_name': 'bacteria.bac', 'database_table': 'bacteria_bac', 'is_core_file': False},
            {'classification': 17, 'order_in_class': 1, 'default_file_name': 'tiledrain.str', 'database_table': 'tiledrain_str', 'is_core_file': True},
            {'classification': 17, 'order_in_class': 2, 'default_file_name': 'septic.str', 'database_table': 'septic_str', 'is_core_file': False},
            {'classification': 17, 'order_in_class': 3, 'default_file_name': 'filterstrip.str', 'database_table': 'filterstrip_str', 'is_core_file': True},
            {'classification': 17, 'order_in_class': 4, 'default_file_name': 'grassedww.str', 'database_table': 'grassedww_str', 'is_core_file': True},
            {'classification': 17, 'order_in_class': 5, 'default_file_name': 'bmpuser.str', 'database_table': 'bmpuser_str', 'is_core_file': False},
            {'classification': 18, 'order_in_class': 1, 'default_file_name': 'plants.plt', 'database_table': 'plants_plt', 'is_core_file': True},
            {'classification': 18, 'order_in_class': 2, 'default_file_name': 'fertilizer.frt', 'database_table': 'fertilizer_frt', 'is_core_file': True},
            {'classification': 18, 'order_in_class': 3, 'default_file_name': 'tillage.til', 'database_table': 'tillage_til', 'is_core_file': True},
            {'classification': 18, 'order_in_class': 4, 'default_file_name': 'pesticide.pst', 'database_table': 'pesticide_pst', 'is_core_file': False},
            {'classification': 18, 'order_in_class': 5, 'default_file_name': 'urban.urb', 'database_table': 'urban_urb', 'is_core_file': True},
            {'classification': 18, 'order_in_class': 6, 'default_file_name': 'septic.sep', 'database_table': 'septic_sep', 'is_core_file': False},
            {'classification': 18, 'order_in_class': 7, 'default_file_name': 'snow.sno', 'database_table': 'snow_sno', 'is_core_file': True},
            {'classification': 19, 'order_in_class': 1, 'default_file_name': 'harv.ops', 'database_table': 'harv_ops', 'is_core_file': True},
            {'classification': 19, 'order_in_class': 2, 'default_file_name': 'graze.ops', 'database_table': 'graze_ops', 'is_core_file': True},
            {'classification': 19, 'order_in_class': 3, 'default_file_name': 'irr.ops', 'database_table': 'irr_ops', 'is_core_file': True},
            {'classification': 19, 'order_in_class': 4, 'default_file_name': 'chem_app.ops', 'database_table': 'chem_app_ops', 'is_core_file': False},
            {'classification': 19, 'order_in_class': 5, 'default_file_name': 'fire.ops', 'database_table': 'fire_ops', 'is_core_file': True},
            {'classification': 19, 'order_in_class': 6, 'default_file_name': 'sweep.ops', 'database_table': 'sweep_ops', 'is_core_file': False},
            {'classification': 20, 'order_in_class': 1, 'default_file_name': 'landuse.lum', 'database_table': 'landuse_lum', 'is_core_file': True},
            {'classification': 20, 'order_in_class': 2, 'default_file_name': 'management.sch', 'database_table': 'management_sch', 'is_core_file': True},
            {'classification': 20, 'order_in_class': 3, 'default_file_name': 'cntable.lum', 'database_table': 'cntable_lum', 'is_core_file': True},
            {'classification': 20, 'order_in_class': 4, 'default_file_name': 'cons_practice.lum', 'database_table': 'cons_practice_lum', 'is_core_file': True},
            {'classification': 20, 'order_in_class': 5, 'default_file_name': 'ovn_table.lum', 'database_table': 'ovn_table_lum', 'is_core_file': True},
            {'classification': 21, 'order_in_class': 1, 'default_file_name': 'codes.cal', 'database_table': 'codes_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 2, 'default_file_name': 'cal_parms.cal', 'database_table': 'cal_parms_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 3, 'default_file_name': 'calibration.cal', 'database_table': 'calibration_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 4, 'default_file_name': 'ls_parms.cal', 'database_table': 'ls_parms_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 5, 'default_file_name': 'ls_regions.cal', 'database_table': 'ls_regions_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 6, 'default_file_name': 'ch_orders.cal', 'database_table': 'ch_orders_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 7, 'default_file_name': 'ch_parms.cal', 'database_table': 'ch_parms_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 8, 'default_file_name': 'pl_parms.cal', 'database_table': 'pl_parms_cal', 'is_core_file': False},
            {'classification': 21, 'order_in_class': 9, 'default_file_name': 'pl_regions.cal', 'database_table': 'pl_regions_cal', 'is_core_file': False},
            {'classification': 22, 'order_in_class': 1, 'default_file_name': 'initial.pst', 'database_table': 'initial_pst', 'is_core_file': False},
            {'classification': 22, 'order_in_class': 2, 'default_file_name': 'initial.plt', 'database_table': 'initial_plt', 'is_core_file': True},
            {'classification': 23, 'order_in_class': 1, 'default_file_name': 'soils.sol', 'database_table': 'soils_sol', 'is_core_file': True},
            {'classification': 23, 'order_in_class': 2, 'default_file_name': 'nutrients.sol', 'database_table': 'nutrients_sol', 'is_core_file': True},
            {'classification': 24, 'order_in_class': 1, 'default_file_name': 'd_table.dtl', 'database_table': 'd_table_dtl', 'is_core_file': True},
            {'classification': 25, 'order_in_class': 1, 'default_file_name': 'constituents.cs', 'database_table': 'constituents_cs', 'is_core_file': False},
            {'classification': 25, 'order_in_class': 2, 'default_file_name': 'pest.cs', 'database_table': 'pest_cs', 'is_core_file': False},
            {'classification': 25, 'order_in_class': 3, 'default_file_name': 'path.cs', 'database_table': 'path_cs', 'is_core_file': False},
            {'classification': 25, 'order_in_class': 4, 'default_file_name': 'hmet.cs', 'database_table': 'hmet_cs', 'is_core_file': False},
            {'classification': 25, 'order_in_class': 5, 'default_file_name': 'salt.cs', 'database_table': 'salt_cs', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 1, 'default_file_name': 'ls_unit.ele', 'database_table': 'ls_unit_ele', 'is_core_file': True},
            {'classification': 26, 'order_in_class': 2, 'default_file_name': 'ls_unit.def', 'database_table': 'ls_unit_def', 'is_core_file': True},
            {'classification': 26, 'order_in_class': 3, 'default_file_name': 'ls_reg.ele', 'database_table': 'ls_reg_ele', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 4, 'default_file_name': 'ls_reg.def', 'database_table': 'ls_reg_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 5, 'default_file_name': 'ls_cal.reg', 'database_table': 'ls_cal_reg', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 6, 'default_file_name': 'ch_catunit.ele', 'database_table': 'ch_catunit_ele', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 7, 'default_file_name': 'ch_catunit.def', 'database_table': 'ch_catunit_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 8, 'default_file_name': 'ch_reg.def', 'database_table': 'ch_reg_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 9, 'default_file_name': 'aqu_catunit.ele', 'database_table': 'aqu_catunit_ele', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 10, 'default_file_name': 'aqu_catunit.def', 'database_table': 'aqu_catunit_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 11, 'default_file_name': 'aqu_reg.def', 'database_table': 'aqu_reg_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 12, 'default_file_name': 'res_catunit.ele', 'database_table': 'res_catunit_ele', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 13, 'default_file_name': 'res_catunit.def', 'database_table': 'res_catunit_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 14, 'default_file_name': 'res_reg.def', 'database_table': 'res_reg_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 15, 'default_file_name': 'rec_catunit.ele', 'database_table': 'rec_catunit_ele', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 16, 'default_file_name': 'rec_catunit.def', 'database_table': 'rec_catunit_def', 'is_core_file': False},
            {'classification': 26, 'order_in_class': 17, 'default_file_name': 'rec_reg.def', 'database_table': 'rec_reg_def', 'is_core_file': False}
        ]

        with base.db.atomic():
            if definitions.File_cio_classification.select().count() < 1:
                definitions.File_cio_classification.insert_many(classifications).execute()

            if definitions.File_cio.select().count() < 1:
                definitions.File_cio.insert_many(file_cio).execute()