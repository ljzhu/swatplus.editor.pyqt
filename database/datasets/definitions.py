from peewee import *
from . import base


class Var_code(base.BaseModel):
    table = CharField()
    variable = CharField()
    code = CharField()
    description = CharField()


class Var_range(base.BaseModel):
    table = CharField()
    variable = CharField()
    type = CharField()
    min_value = DoubleField()
    max_value = DoubleField()
    default_value = DoubleField()
    default_text = CharField(null=True)
    units = CharField(null=True)
    description = CharField(null=True)


class File_cio_classification(base.BaseModel):
    name = CharField()


class File_cio(base.BaseModel):
    classification = ForeignKeyField(File_cio_classification, on_delete='CASCADE', related_name='files')
    order_in_class = IntegerField()
    database_table = CharField()
    default_file_name = CharField()
    is_core_file = BooleanField()


"""class File_cio(base.BaseModel):
    section = CharField()
    default_file_names = TextField()
    txt_file_names = TextField()"""


class ColumnTypes:
    INT = "int"
    DOUBLE = "double"
    STRING = "string"


class TableNames:
    HYDROLOGY_CHA = "hydology_cha"
    TOPOGRAPHY_SUB = "topography_sub"
