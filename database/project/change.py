from peewee import *
from . import base


class Codes_cal(base.BaseModel):
    landscape = BooleanField()
    hyd = BooleanField()
    plnt = BooleanField()
    sed = BooleanField()
    nut = BooleanField()
    ch_sed = BooleanField()
    ch_nut = BooleanField()
    res = BooleanField()


class Cal_parms_cal(base.BaseModel):
    name = CharField()
    obj_typ = CharField()
    abs_min = DoubleField()
    abs_max = DoubleField()
    units = IntegerField()


class Calibration_cal(base.BaseModel):
    name = CharField()
    chg_typ = CharField()  # absval, abschg, pctchg
    chg_val = DoubleField()
    cond_tot = IntegerField()
    soil_lyr1 = IntegerField()
    soil_lyr2 = IntegerField()
    yr1 = IntegerField()
    yr2 = IntegerField()
    day1 = IntegerField()
    day2 = IntegerField()
    numb_tot = IntegerField()
    elem_cnt = IntegerField()
    numb_cond = IntegerField()


class Ls_parms_cal(base.BaseModel):
    name = CharField()
    chg_typ = CharField()  # absval, abschg, pctchg
    neg = DoubleField()
    pos = DoubleField()
    lo = DoubleField()
    up = DoubleField()


class Ls_regions_cal(base.BaseModel):
    name = CharField()
    lum_numb = IntegerField()
    nspu = IntegerField()
    elem_cnt = IntegerField()
    cal_name = CharField()
    surq_rto = DoubleField()
    latq_rto = DoubleField()
    perc_rto = DoubleField()
    et_rto = DoubleField()
    tileq_rto = DoubleField()
    sed = DoubleField()
    orgn = DoubleField()
    orgp = DoubleField()
    no3 = DoubleField()
    solp = DoubleField()


class Ch_orders_cal(base.BaseModel):
    name = CharField()
    cha_wide = DoubleField()
    cha_dc_accr = DoubleField()
    head_cut = DoubleField()
    fp_accr = DoubleField()


class Ch_parms_cal(base.BaseModel):
    name = CharField()
    chg_typ = CharField()  # absval, abschg, pctchg
    neg = DoubleField()
    pos = DoubleField()
    lo = DoubleField()
    up = DoubleField()


class Pl_parms_cal(base.BaseModel):
    name = CharField()
    chg_typ = CharField()  # absval, abschg, pctchg
    neg = DoubleField()
    pos = DoubleField()
    lo = DoubleField()
    up = DoubleField()


class Pl_regions_cal(base.BaseModel):
    name = CharField()
