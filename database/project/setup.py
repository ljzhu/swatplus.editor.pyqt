from peewee import *
from . import base, config, simulation, climate, link, channel, reservoir, dr, exco, hydrology, routing_unit, aquifer, \
    basin, bacteria, parm_db, structural, ops, decision_table, init, lum, soils, \
    change, constituents, regions, hru, connect, gis
from database import lib
from database.datasets import base as datasets_base, definitions as dataset_defs

class SetupProjectDatabase():
    @staticmethod
    def create_tables():
        base.db.create_tables([config.Project_config, config.File_cio_classification, config.File_cio], True)
        base.db.create_tables([simulation.Time_sim, simulation.Print_prt, simulation.Print_prt_aa_int, simulation.Print_prt_object, simulation.Object_prt, simulation.Object_cnt], True)
        base.db.create_tables([climate.Weather_wgn_cli, climate.Weather_wgn_cli_mon, climate.Weather_sta_cli, climate.Weather_file, climate.Wind_dir_cli, climate.Atmodep_cli], True)
        base.db.create_tables([link.Chan_aqu_lin, link.Chan_aqu_lin_ob, link.Chan_surf_lin, link.Chan_surf_lin_ob], True)
        base.db.create_tables([channel.Initial_cha, channel.Hydrology_cha, channel.Sediment_cha, channel.Nutrients_cha, channel.Pesticide_cha, channel.Channel_cha, channel.Channel_lte_cha], True)
        base.db.create_tables([reservoir.Initial_res, reservoir.Hydrology_res, reservoir.Sediment_res, reservoir.Nutrients_res, reservoir.Pesticide_res, reservoir.Weir_res, reservoir.Reservoir_res, reservoir.Hydrology_wet, reservoir.Wetland_wet], True)
        base.db.create_tables([dr.Delratio_del], True)
        base.db.create_tables([exco.Exco_exc, exco.Recall_rec, exco.Recall_dat], True)
        base.db.create_tables([hydrology.Topography_hyd, hydrology.Hydrology_hyd, hydrology.Field_fld], True)
        base.db.create_tables([routing_unit.Rout_unit_dr, routing_unit.Rout_unit_rtu, routing_unit.Rout_unit_ele], True)
        base.db.create_tables([aquifer.Aquifer_aqu], True)
        base.db.create_tables([basin.Codes_bsn, basin.Parameters_bsn], True)
        base.db.create_tables([bacteria.Bacteria_bac, bacteria.Initial_bac], True)
        base.db.create_tables([parm_db.Plants_plt, parm_db.Fertilizer_frt, parm_db.Tillage_til, parm_db.Pesticide_pst, parm_db.Urban_urb, parm_db.Septic_sep, parm_db.Snow_sno], True)
        base.db.create_tables([structural.Septic_str, structural.Bmpuser_str, structural.Filterstrip_str, structural.Grassedww_str, structural.Tiledrain_str], True)
        base.db.create_tables([ops.Graze_ops, ops.Harv_ops, ops.Irr_ops, ops.Chem_app_ops, ops.Fire_ops, ops.Sweep_ops], True)
        base.db.create_tables([decision_table.D_table_dtl, decision_table.D_table_dtl_cond, decision_table.D_table_dtl_cond_alt, decision_table.D_table_dtl_act, decision_table.D_table_dtl_act_out], True)
        base.db.create_tables([init.Initial_plt, init.Initial_plt_item, init.Initial_pst], True)
        base.db.create_tables([lum.Management_sch, lum.Management_sch_op, lum.Cntable_lum, lum.Cons_prac_lum, lum.Ovn_table_lum, lum.Landuse_lum], True)
        base.db.create_tables([soils.Soils_sol, soils.Soils_sol_layer, soils.Nutrients_sol], True)
        base.db.create_tables([change.Codes_cal, change.Cal_parms_cal, change.Calibration_cal, change.Ls_parms_cal, change.Ls_regions_cal, change.Ch_orders_cal, change.Ch_parms_cal, change.Pl_parms_cal, change.Pl_regions_cal], True)
        base.db.create_tables([constituents.Constituents_cs, constituents.Pest_cs, constituents.Path_cs, constituents.Hmet_cs, constituents.Salt_cs], True)

        base.db.create_tables([regions.Ls_unit_ele, regions.Ls_unit_def, regions.Ls_unit_def_elem,
                               regions.Ls_reg_ele, regions.Ls_reg_def, regions.Ls_reg_def_elem,
                               regions.Ch_catunit_ele, regions.Ch_catunit_def, regions.Ch_catunit_def_elem, regions.Ch_reg_def, regions.Ch_reg_def_elem,
                               regions.Aqu_catunit_ele, regions.Aqu_catunit_def, regions.Aqu_catunit_def_elem, regions.Aqu_reg_def, regions.Aqu_reg_def_elem,
                               regions.Res_catunit_ele, regions.Res_catunit_def, regions.Res_catunit_def_elem, regions.Res_reg_def, regions.Res_reg_def_elem,
                               regions.Rec_catunit_ele, regions.Rec_catunit_def, regions.Rec_catunit_def_elem, regions.Rec_reg_def, regions.Rec_reg_def_elem], True)

        base.db.create_tables([hru.Hru_lte_hru, hru.Hru_data_hru], True)

        base.db.create_tables([connect.Hru_con, connect.Hru_con_out,
                               connect.Hru_lte_con, connect.Hru_lte_con_out,
                               connect.Rout_unit_con, connect.Rout_unit_con_out,
                               connect.Modflow_con, connect.Modflow_con_out,
                               connect.Aquifer_con, connect.Aquifer_con_out,
                               connect.Aquifer2d_con, connect.Aquifer2d_con_out,
                               connect.Channel_con, connect.Channel_con_out,
                               connect.Reservoir_con, connect.Reservoir_con_out,
                               connect.Recall_con, connect.Recall_con_out,
                               connect.Exco_con, connect.Exco_con_out,
                               connect.Delratio_con, connect.Delratio_con_out,
                               connect.Outlet_con, connect.Outlet_con_out,
                               connect.Chandeg_con, connect.Chandeg_con_out], True)

        base.db.create_tables([gis.Gis_channels, gis.Gis_subbasins, gis.Gis_hrus, gis.Gis_lsus, gis.Gis_water, gis.Gis_points, gis.Gis_routing], True)

    @staticmethod
    def copy_default_datasets():
        datasets_db_name = datasets_base.db.database
        project_db_name = base.db.database

        if parm_db.Plants_plt.select().count() < 1:
            lib.copy_table('plants_plt', datasets_db_name, project_db_name)

        if parm_db.Fertilizer_frt.select().count() < 1:
            lib.copy_table('fertilizer_frt', datasets_db_name, project_db_name)

        if parm_db.Tillage_til.select().count() < 1:
            lib.copy_table('tillage_til', datasets_db_name, project_db_name)

        if parm_db.Pesticide_pst.select().count() < 1:
            lib.copy_table('pesticide_pst', datasets_db_name, project_db_name)

        if parm_db.Urban_urb.select().count() < 1:
            lib.copy_table('urban_urb', datasets_db_name, project_db_name)

        if parm_db.Septic_sep.select().count() < 1:
            lib.copy_table('septic_sep', datasets_db_name, project_db_name)

        if config.File_cio_classification.select().count() < 1:
            #lib.copy_table('file_cio_classification', datasets_db_name, project_db_name)
            class_query = dataset_defs.File_cio_classification.select()
            if class_query.count() > 0:
                classes = []
                for f in class_query:
                    cl = {
                        'name': f.name
                    }
                    classes.append(cl)

                lib.bulk_insert(base.db, config.File_cio_classification, classes)

            file_cio_query = dataset_defs.File_cio.select()
            if file_cio_query.count() > 0:
                file_cios = []
                for f in file_cio_query:
                    file_cio = {
                        'classification': f.classification.id,
                        'order_in_class': f.order_in_class,
                        'file_name': f.default_file_name
                    }
                    file_cios.append(file_cio)

                lib.bulk_insert(base.db, config.File_cio, file_cios)


