from peewee import *
from . import base
from . import hydrology
from . import parm_db
from . import soils
from . import lum


class Hru_data_hru(base.BaseModel):
    name = CharField()
    topo = ForeignKeyField(hydrology.Topography_hyd, null=True, on_delete='SET NULL')
    hydro = ForeignKeyField(hydrology.Hydrology_hyd, null=True, on_delete ='SET NULL')
    soil = ForeignKeyField(soils.Soils_sol, null=True, on_delete='SET NULL')
    lu_mgt = ForeignKeyField(lum.Landuse_lum, null=True, on_delete ='SET NULL')
    soil_nut = ForeignKeyField(soils.Nutrients_sol, null=True, on_delete='SET NULL')
    surf_stor = CharField(null=True)  # Undefined pointer to something
    snow = ForeignKeyField(parm_db.Snow_sno, null=True, on_delete='SET NULL')
    field = ForeignKeyField(hydrology.Field_fld, null=True, on_delete='SET NULL')
    description = TextField(null=True)


class Hru_lte_hru(base.BaseModel):
    area = DoubleField()
    cn2 = DoubleField()
    cn3_swf = DoubleField()
    t_conc = DoubleField()
    soil_dp = DoubleField()
    perc_co = DoubleField()
    slp = DoubleField()
    slp_len = DoubleField()
    et_co = DoubleField()
    aqu_sp_yld = DoubleField()
    alpha_bf = DoubleField()
    revap = DoubleField()
    rchg_dp = DoubleField()
    sw_init = DoubleField()
    aqu_init = DoubleField()
    aqu_sh_flo = DoubleField()
    aqu_dp_flo = DoubleField()
    snow_h2o = DoubleField()
    lat = DoubleField()
    soil_text = IntegerField()
    trop_flag = IntegerField()
    grow_start = IntegerField()
    grow_end = IntegerField()
    plnt_typ = CharField(null=True)
    stress = DoubleField()
    pet_flag = IntegerField()
    irr_flag = IntegerField()
    irr_src = IntegerField()
    t_drain = DoubleField()
    usle_k = DoubleField()
    usle_c = DoubleField()
    usle_p = DoubleField()
    usle_ls = DoubleField()
