from peewee import *
from . import base


class Bacteria_bac(base.BaseModel):
    name = CharField()
    die_sol = DoubleField()
    grow_sol = DoubleField()
    die_srb = DoubleField()
    grow_srb = DoubleField()
    sol_srb = DoubleField()
    tmp_adj = DoubleField()
    washoff = DoubleField()
    die_plnt = DoubleField()
    grow_plnt = DoubleField()
    frac_man = DoubleField()
    perc_sol = DoubleField()
    detect = DoubleField()
    die_cha = DoubleField()
    grow_cha = DoubleField()
    die_res = DoubleField()
    grow_res = DoubleField()
    swf = DoubleField()
    conc_min = DoubleField()


class Initial_bac(base.BaseModel):
    numb_db = IntegerField() # Not sure if this is a FK to bacteria_bac or not...
    plnt_bact = DoubleField()
    sol_bact = DoubleField()
    srb_bact = DoubleField()
