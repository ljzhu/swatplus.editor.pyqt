from peewee import *
from . import base
from . import parm_db


class Graze_ops(base.BaseModel):
    name = CharField()
    fert = ForeignKeyField(parm_db.Fertilizer_frt, on_delete='CASCADE')
    bm_eat = DoubleField()
    bm_tramp = DoubleField()
    man_amt = DoubleField()
    grz_bm_min = DoubleField()
    description = TextField(null=True)


class Harv_ops(base.BaseModel):
    name = CharField()
    harv_typ = CharField()  # grain, biomass, residue, tree, tuber
    harv_idx = DoubleField()
    harv_eff = DoubleField()
    harv_bm_min = DoubleField()
    description = TextField(null=True)


class Irr_ops(base.BaseModel):
    name = CharField()
    irr_eff = DoubleField()
    surq_rto = DoubleField()
    irr_amt = DoubleField()
    irr_salt = DoubleField()
    irr_no3n = DoubleField()
    irr_po4n = DoubleField()
    description = TextField(null=True)


class Sweep_ops(base.BaseModel):
    name = CharField()
    swp_eff = DoubleField()
    frac_curb = DoubleField()
    description = TextField(null=True)


class Fire_ops(base.BaseModel):
    name = CharField()
    chg_cn2 = DoubleField()
    frac_burn = DoubleField()
    description = TextField(null=True)


class Chem_app_ops(base.BaseModel):
    name = CharField()
    chem_form = CharField()  # solid, liquid
    app_typ = CharField()  # spread, spray, inject, direct
    app_eff = DoubleField()
    foliar_eff = DoubleField()
    inject_dp = DoubleField()
    surf_frac = DoubleField()
    drift_pot = DoubleField()
    aerial_unif = DoubleField()
    description = TextField(null=True)
