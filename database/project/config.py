from peewee import *
from . import base


class File_cio_classification(base.BaseModel):
    name = CharField()


class File_cio(base.BaseModel):
    classification = ForeignKeyField(File_cio_classification, on_delete='CASCADE', related_name='files')
    order_in_class = IntegerField()
    file_name = CharField()


class Project_config(base.BaseModel):
    project_name = CharField(null=True)
    project_directory = CharField(null=True)
    editor_version = CharField()
    gis_type = CharField(null=True)
    gis_version = CharField(null=True)
    project_db = CharField(null=True)
    reference_db = CharField(null=True)
    wgn_db = CharField(null=True)
    wgn_table_name = CharField(null=True)
    weather_data_dir = CharField(null=True)
    weather_data_format = CharField(null=True)
    input_files_dir = CharField(null=True)
    input_files_last_written = DateTimeField(null=True)
    swat_last_run = DateTimeField(null=True)
    delineation_done = BooleanField()
    hrus_done = BooleanField()

    @classmethod
    def update_version(cls, version):
        q = cls.update(editor_version=version)
        return q.execute()

    @classmethod
    def update_wgn(cls, database, table):
        q = cls.update(wgn_db=database, wgn_table_name=table)
        return q.execute()

    @classmethod
    def update_weather_data(cls, dir, format):
        q = cls.update(weather_data_dir=dir, weather_data_format=format)
        return q.execute()

    @classmethod
    def update_input_files_dir(cls, dir):
        q = cls.update(input_files_dir=dir)
        return q.execute()

    @classmethod
    def update_input_files_written(cls, date):
        q = cls.update(input_files_last_written=date)
        return q.execute()

    @classmethod
    def update_swat_run(cls, date):
        q = cls.update(swat_last_run=date)
        return q.execute()

    @classmethod
    def get_or_create_default(cls, new_version=None):
        if cls.select().count() > 0:
            return cls.get()

        return cls.create(editor_version=new_version, delineation_done=False, hrus_done=False)