from peewee import *
from .base import BaseModel

class Time_sim(BaseModel):
    jd_start = IntegerField()
    yr_start = IntegerField()
    jd_end = IntegerField()
    yr_end = IntegerField()
    step = IntegerField()

    @classmethod
    def update_and_exec(cls, jd_start, yr_start, jd_end, yr_end, step):
        if cls.select().count() > 0:
            q = cls.update(jd_start=jd_start, yr_start=yr_start, jd_end=jd_end, yr_end=yr_end, step=step)
            return q.execute()
        else:
            v = cls.create(jd_start=jd_start, yr_start=yr_start, jd_end=jd_end, yr_end=yr_end, step=step)
            return 1 if v is not None else 0

    @classmethod
    def get_or_create_default(cls):
        if cls.select().count() > 0:
            return cls.get()

        return cls.create(jd_start=1, yr_start=1980, jd_end=1, yr_end=1985, step=0)


class Print_prt(BaseModel):
    nyskip = IntegerField()
    jd_start = IntegerField()
    jd_end = IntegerField()
    yr_start = IntegerField()
    yr_end = IntegerField()
    interval = IntegerField()
    csvout = BooleanField()
    dbout = BooleanField()
    cdfout = BooleanField()
    solout = BooleanField()
    mgtout = BooleanField()
    hydcon = BooleanField()
    fdcout = BooleanField()


class Print_prt_aa_int(BaseModel):
    print_prt = ForeignKeyField(Print_prt, on_delete='CASCADE')
    year = IntegerField()


class Print_prt_object(BaseModel):
    print_prt = ForeignKeyField(Print_prt, on_delete='CASCADE')
    name = CharField()
    daily = BooleanField()
    monthly = BooleanField()
    yearly = BooleanField()
    avann = BooleanField()


class Object_prt(BaseModel):
    obtyp = CharField()
    obtypno = IntegerField()
    hydtyp = CharField()
    filename = CharField()


class Object_cnt(BaseModel):
    objs = IntegerField()
    hru = IntegerField()
    hru_lte = IntegerField()
    sub = IntegerField()
    modflow = IntegerField()
    aqu = IntegerField()
    chan = IntegerField()
    res = IntegerField()
    recall = IntegerField()
    exco = IntegerField()
    dr = IntegerField()
    canal = IntegerField()
    pump = IntegerField()
    outlet = IntegerField()
    chandeg = IntegerField()
    aqu2d = IntegerField()
    herd = IntegerField()
    wro = IntegerField()
