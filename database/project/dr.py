from peewee import *
from . import base


class Delratio_del(base.BaseModel):
    name = CharField()
    flo = DoubleField()
    sed = DoubleField()
    mass = DoubleField()
    cbn = DoubleField()
    ptl_n = DoubleField()
    ptl_p = DoubleField()
    m2 = DoubleField()
    no3_n = DoubleField()
    no2_n = DoubleField()
    nh4_n = DoubleField()
    po4_p = DoubleField()
    chla = DoubleField()
    bod = DoubleField()
    oxy = DoubleField()
    tmp = DoubleField()
    sand = DoubleField()
    silt = DoubleField()
    clay = DoubleField()
    sm_agg = DoubleField()
    lg_agg = DoubleField()
    gravel = DoubleField()
