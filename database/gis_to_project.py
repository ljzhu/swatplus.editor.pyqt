from peewee import *
import os.path
import sqlite3

from database.project import base as project_base, routing_unit, channel, connect, aquifer, hydrology, hru, reservoir, soils, init, lum, parm_db
from database import gis
from database.datasets import base as datasets_base
from database import lib as db_lib


class GisToProject:
    def __init__(self, project_db, gis_db, datasets_db):
        project_base.db.init(project_db)
        gis.db.init(gis_db)
        datasets_base.db.init(datasets_db)

        self.project_db_name = project_db
        self.gis_db_name = gis_db
        self.datasets_db_name = datasets_db

        self.project_db = project_base.db
        self.gis_db = gis.db
        self.datasets_db = datasets_base.db

    def insert_default(self):
        self.insert_subbasins()
        self.insert_reservoirs()
        self.insert_recall()
        self.insert_hrus()
    
    def insert_subbasins(self):
        """
        Insert subbasin, channel, and aquifer SWAT+ tables from GIS database.
        """
        if routing_unit.Rout_unit_rtu.select().count() > 0:
            raise ValueError('Your project database already has data imported from GIS.')

        hydrology_chas = []
        channel_chas = []
        channel_cons = []
        channel_con_outs = []

        aquifer_aqus = []
        aquifer_cons = []
        aquifer_con_outs = []

        topography_subs = []
        subbasin_subs = []
        subbasin_cons = []
        subbasin_con_outs = []

        '''
        Doing a manual query here so we don't have to do multiple loops. 
        The GIS database doesn't have natural FKs set up due to DRAINCATs, so this is necessary.
        Trying to make it at least a bit more readable using col.index('COLNAME') instead of integer positions.
        '''
        cursor = self.gis_db.execute_sql("select s.ID, p.DRAIN, s.AREA, s.SLO1, s.SLL, s.LAT, s.LON, s.ELEV, r.WID2, r.DEP2, r.SLO2, r.LEN2, r.AREAC from subbasins s inner join reaches r on s.ID = r.ID inner join points p on s.ID = p.SUBBASIN where p.PTYPE = 'O' order by s.ID")
        ncols = len(cursor.description)
        col = [cursor.description[i][0] for i in range(ncols)]

        topo_id = 1
        for row in cursor.fetchall():
            id = row[col.index('ID')]
            cha_name = 'cha%s' % id
            sub_name = 'sub%s' % id
            aqu_name = 'aqu%s' % id
            lat = row[col.index('LAT')]
            long = row[col.index('LON')]
            area = row[col.index('AREA')]

            # Channel tables
            hyd_cha = {
                'id': id,
                'name': cha_name,
                'w': row[col.index('WID2')],
                'd': row[col.index('DEP2')],
                's': row[col.index('SLO2')],
                'l': row[col.index('LEN2')],
                'n': 0.5,
                'k': 0,
                'wdr': 6,
                'alpha_bnk': 0,
                'side': 0
            }
            hydrology_chas.append(hyd_cha)

            chan_cha = {
                'id': id,
                'name': cha_name,
                'hyd': id
            }
            channel_chas.append(chan_cha)

            chan_con = {
                'cha': id,
                'id': id,
                'name': cha_name,
                'lat': lat,
                'long': long,
                'area_ha': row[col.index('AREAC')],
                'overflow': 0,
                'ruleset': 0
            }
            channel_cons.append(chan_con)

            drain = row[col.index('DRAIN')]
            if drain != -1:  # If -1, it is the watershed outlet and there is no connection
                chan_con_out = {
                    'channel_con': id,
                    'order': 1,
                    'obtyp_out': 'cha',
                    'obtyno_out': drain,
                    'hytyp_out': 'tot',
                    'frac_out': 1.0
                }
                channel_con_outs.append(chan_con_out)

            # Aquifer tables
            aqu = {
                'id': id,
                'name': aqu_name,
                'flo': 2500,
                'stor': 1000,
                'hgt': 1,
                'no3': 0,
                'minp': 0,
                'orgn': 0,
                'orgp': 0,
                'delay': 31,
                'alpha': 0.048,
                'revap': 0.02,
                'seep': 0.05,
                'spyld': 0.003,
                'hlife_n': 0,
                'flo_min': 1000,
                'revap_min': 750
            }
            aquifer_aqus.append(aqu)

            aqu_con = {
                'aqu': id,
                'id': id,
                'name': aqu_name,
                'lat': lat,
                'long': long,
                'area_ha': area,
                'overflow': 0,
                'ruleset': 0
            }
            aquifer_cons.append(aqu_con)

            aqu_con_out = {
                'aquifer_con': id,
                'order': 1,
                'obtyp_out': 'cha',
                'obtyno_out': id,
                'hytyp_out': 'tot',
                'frac_out': 1.0
            }
            aquifer_con_outs.append(aqu_con_out)

            # Subbasin tables
            topo = {
                'id': topo_id,
                'name': sub_name,
                'slope': row[col.index('SLO1')] / 100,
                'slope_len': row[col.index('SLL')],
                'lat_len': 50.0,
                'dis_stream': 100.0,
                'dep_co': 0.5,
                'type': 'sub'
            }
            topography_subs.append(topo)

            sub = {
                'id': id,
                'name': sub_name,
                'topo_hyd': topo_id
            }
            subbasin_subs.append(sub)
            topo_id += 1

            sub_con = {
                'ru': id,
                'id': id,
                'name': sub_name,
                'elev': row[col.index('ELEV')],
                'lat': lat,
                'long': long,
                'area_ha': area,
                'overflow': 0,
                'ruleset': 0
            }
            subbasin_cons.append(sub_con)

            sub_con_out_cha = {
                'ru_con': id,
                'order': 1,
                'obtyp_out': 'cha',
                'obtyno_out': id,
                'hytyp_out': 'tot',
                'frac_out': 1.0
            }

            sub_con_out_aqu = {
                'ru_con': id,
                'order': 2,
                'obtyp_out': 'aqu',
                'obtyno_out': id,
                'hytyp_out': 'rhg',
                'frac_out': 1.0
            }

            subbasin_con_outs.append(sub_con_out_cha)
            subbasin_con_outs.append(sub_con_out_aqu)

        db_lib.bulk_insert(self.project_db, channel.Hydrology_cha, hydrology_chas)
        db_lib.bulk_insert(self.project_db, channel.Channel_cha, channel_chas)
        db_lib.bulk_insert(self.project_db, connect.Channel_con, channel_cons)
        db_lib.bulk_insert(self.project_db, connect.Channel_con_out, channel_con_outs)

        db_lib.bulk_insert(self.project_db, aquifer.Aquifer_aqu, aquifer_aqus)
        db_lib.bulk_insert(self.project_db, connect.Aquifer_con, aquifer_cons)
        db_lib.bulk_insert(self.project_db, connect.Aquifer_con_out, aquifer_con_outs)

        db_lib.bulk_insert(self.project_db, hydrology.Topography_hyd, topography_subs)
        db_lib.bulk_insert(self.project_db, routing_unit.Rout_unit_rtu, subbasin_subs)
        db_lib.bulk_insert(self.project_db, connect.Rout_unit_con, subbasin_cons)
        db_lib.bulk_insert(self.project_db, connect.Rout_unit_con_out, subbasin_con_outs)

    def insert_reservoirs(self):
        """
        Insert reservoir SWAT+ tables from GIS database.
        """
        if reservoir.Reservoir_res.select().count() > 0:
            raise ValueError('Your project database already has reservoir data imported from GIS.')

        res_query = gis.Points.select().where(gis.Points.PTYPE == 'R').order_by(gis.Points.ID)
        if res_query.count() > 0:
            reservoir_res = []
            reservoir_cons = []
            reservoir_con_outs = []
            res_num = 1
            for gis_res in res_query:
                id = res_num
                res_name = 'res%s' % res_num

                res = {
                    'id': id,
                    'name': res_name,
                    'release': 0
                }
                reservoir_res.append(res)

                res_con = {
                    'res': id,
                    'id': id,
                    'name': res_name,
                    'lat': gis_res.LAT,
                    'long': gis_res.LON,
                    'area_ha': 0,
                    'overflow': 0,
                    'ruleset': 0
                }
                reservoir_cons.append(res_con)

                res_con_out = {
                    'reservoir_con': id,
                    'order': 1,
                    'obtyp_out': 'cha',
                    'obtyno_out': gis_res.SUBBASIN,
                    'hytyp_out': 'tot',
                    'frac_out': 1.0
                }
                reservoir_con_outs.append(res_con_out)

                res_num += 1

            db_lib.bulk_insert(self.project_db, reservoir.Reservoir_res, reservoir_res)
            db_lib.bulk_insert(self.project_db, connect.Reservoir_con, reservoir_cons)
            db_lib.bulk_insert(self.project_db, connect.Reservoir_con_out, reservoir_con_outs)

    def insert_recall(self):
        """
        Insert recall (point source) SWAT+ tables from GIS database.
        """
        if connect.Recall_rec.select().count() > 0:
            raise ValueError('Your project database already has recall data imported from GIS.')

        rec_query = gis.Points.select().where((gis.Points.PTYPE == 'P') | (gis.Points.PTYPE == 'I')).order_by(gis.Points.ID)
        if rec_query.count() > 0:
            recall_rec = []
            recall_cons = []
            recall_con_outs = []
            rec_num = 1
            for gis_rec in rec_query:
                id = rec_num
                rec_name = 'rec%s' % rec_num

                rec = {
                    'id': id,
                    'name': rec_name,
                    'type': 1,
                    'filename': '%s.dat' % rec_name
                }
                recall_rec.append(rec)

                rec_con = {
                    'rec': id,
                    'id': id,
                    'name': rec_name,
                    'lat': gis_rec.LAT,
                    'long': gis_rec.LON,
                    'area_ha': 0,
                    'overflow': 0,
                    'ruleset': 0
                }
                recall_cons.append(rec_con)

                rec_con_out = {
                    'recall_con': id,
                    'order': 1,
                    'obtyp_out': 'cha',
                    'obtyno_out': gis_rec.SUBBASIN,
                    'hytyp_out': 'tot',
                    'frac_out': 1.0
                }
                recall_con_outs.append(rec_con_out)

                rec_num += 1

            db_lib.bulk_insert(self.project_db, connect.Recall_rec, recall_rec)
            db_lib.bulk_insert(self.project_db, connect.Recall_con, recall_cons)
            db_lib.bulk_insert(self.project_db, connect.Recall_con_out, recall_con_outs)

    def insert_soil(self):
        """
        Insert soils.sol SWAT+ data from GIS database. 
        Currently only adding soils used in HRUs to the soils.sol table.
        Not sure yet if it would be better to just add them all.
        """
        if soils.Soils_sol.select().count() > 0:
            raise ValueError('Your project database already has soils data imported from GIS.')

        distinct_soils = gis.Hrus.select(gis.Hrus.soil_id).order_by(gis.Hrus.soil_id).distinct()
        soil_ids = [item.soil_id for item in distinct_soils]

        # Chunk the id array so we don't hit the SQLite parameter limit!
        max_length = 999
        soil_id_chunks = [soil_ids[i:i + max_length] for i in range(0, len(soil_ids), max_length)]

        settings = gis.Project.get()

        gis_db_path = os.path.dirname(self.gis_db_name)
        soils_db_file = ''
        if os.path.exists(settings.soils_db):
            soils_db_file = settings.soils_db
        elif os.path.exists(os.path.join(gis_db_path, settings.soils_db)):
            soils_db_file = os.path.join(gis_db_path, settings.soils_db)
        else:
            raise ValueError("Could not find the soils database file, {soilsdb}, listed in the {gisdb} project table.".format(soilsdb=settings.soils_db, gisdb=self.gis_db_name))

        conn = sqlite3.connect(soils_db_file)
        conn.row_factory = sqlite3.Row

        layer_table = "{}_layer".format(settings.soils_table)
        if not db_lib.exists_table(conn, settings.soils_table):
            raise ValueError(
                "Table {table} does not exist in {file}.".format(table=settings.soils_table, file=soils_db_file))

        if not db_lib.exists_table(conn, layer_table):
            raise ValueError(
                "Table {table} does not exist in {file}.".format(table=layer_table, file=soils_db_file))

        for chunk in soil_id_chunks:
            soils_list = []
            layers = []

            query = conn.cursor().execute('select * from %s where id in (%s)' % (settings.soils_table, ','.join('?'*len(chunk))), chunk)
            for row in query.fetchall():
                soil = {
                    'id': row['id'],
                    'name': row['name'],
                    'hydgrp': row['hydgrp'],
                    'zmx': row['zmx'],
                    'anion_excl': row['anion_excl'],
                    'crk': row['crk'],
                    'texture': row['texture']
                }
                soils_list.append(soil)

                layer_query = conn.cursor().execute('select * from %s where soil_id = ?' % layer_table, (row['id'],))
                for layer_row in layer_query.fetchall():
                    layer = {
                        'soil': row['id'],
                        'layer_num': layer_row['layer_num'],
                        'z': layer_row['z'],
                        'bd': layer_row['bd'],
                        'awc': layer_row['awc'],
                        'k': layer_row['k'],
                        'cbn': layer_row['cbn'],
                        'clay': layer_row['clay'],
                        'silt': layer_row['silt'],
                        'sand': layer_row['sand'],
                        'rock': layer_row['rock'],
                        'alb': layer_row['alb'],
                        'usle_k': layer_row['usle_k'],
                        'ec': layer_row['ec'],
                        'cal': layer_row['cal'],
                        'ph': layer_row['ph']
                    }
                    layers.append(layer)

            db_lib.bulk_insert(self.project_db, soils.Soils_sol, soils_list)
            db_lib.bulk_insert(self.project_db, soils.Soils_sol_layer, layers)

    def insert_landuse(self):
        """
        Insert initial.plt and landuse.lum SWAT+ data from GIS database.
        Currently adding a plant community for each distinct landuse in the GIS hrus table.
        Currently using text LANDUSE column, but may change it to an id integer column later.
        :returns: Dictionary of landuse.lum IDs to landuse name
        """
        if lum.Landuse_lum.select().count() > 0:
            raise ValueError('Your project database already has landuse data imported from GIS.')

        distinct_lu = gis.Hrus.select(gis.Hrus.LANDUSE).distinct()
        lus = [item.LANDUSE.lower() for item in distinct_lu]

        project_plants = parm_db.Plants_plt.select().where(parm_db.Plants_plt.name << lus)
        project_urbans = parm_db.Urban_urb.select().where(parm_db.Urban_urb.name << lus)

        plant_coms = []
        plant_com_items = []
        plant_com_id = 1
        for plt in project_plants:
            plant_com = {
                'id': plant_com_id,
                'name': '{name}_comm'.format(name=plt.name)
            }
            plant_coms.append(plant_com)

            plant_com_item = {
                'initial_plt': plant_com_id,
                'cpnm': plt.id,
                'igro': 1,
                'phu': 2000,
                'lai': 0,
                'bioms': 0,
                'phuacc': 0,
                'pop': 0,
                'yrmat': 0,
                'rsdin': 10000
            }
            plant_com_items.append(plant_com_item)

            plant_com_id += 1

        db_lib.bulk_insert(self.project_db, init.Initial_plt, plant_coms)
        db_lib.bulk_insert(self.project_db, init.Initial_plt_item, plant_com_items)

        lums = []
        lum_dict = {}
        lum_id = 1
        for pcom in init.Initial_plt.select().order_by(init.Initial_plt.id):
            plant_name = pcom.name.strip().split('_')[0]
            l = {
                'id': lum_id,
                'name': '{name}_lum'.format(name=plant_name),
                'plant_cov': pcom.id
            }
            lums.append(l)

            lum_dict[plant_name] = lum_id

            lum_id += 1

        db_lib.bulk_insert(self.project_db, lum.Landuse_lum, lums)

        urb_lums = []
        for urb in project_urbans:
            l = {
                'id': lum_id,
                'name': '{name}_lum'.format(name=urb.name),
                'urb_lu': urb.id,
                'urb_ro': 'buildup_washoff'
            }
            urb_lums.append(l)

            lum_dict[urb.name] = lum_id

            lum_id += 1

        db_lib.bulk_insert(self.project_db, lum.Landuse_lum, urb_lums)

        return lum_dict

    def insert_hrus(self):
        """
        Insert hru_data.hru SWAT+ data from GIS database.
        """
        self.insert_soil()
        lum_dict = self.insert_landuse()

        # Create default hydrology.hyd
        hyd = hydrology.Hydrology_hyd.create(
            name='hyd1',
            lat_ttime=0.00,
            lat_sed=0.00,
            canmx=1.00,
            esco=0.95,
            epco=1.00,
            erorgn=0.00,
            erorgp=0.00,
            cn3_swf=0.50,
            biomix=0.20,
            dep_imp=0.00,
            lat_orgn=0.00,
            lat_orgp=0.00,
            harg_pet=0.00,
            cncoef=1.00
        )

        # Create default nutrients.sol
        nut = soils.Nutrients_sol.create(
            name='soilnut1',
            exp_co=13.00,
            totaln=6.00,
            inorgn=3.00,
            orgn=3.00,
            totalp=3.50,
            inorgp=0.40,
            orgp=0.15,
            watersol_p=0.25,
            h3a_p=1.20,
            mehlich_p=0.85,
            bray_strong_p=0.85
        )

        hrus = []
        topos = []
        hru_cons = []
        elem_subs = []
        topo_id = hydrology.Topography_hyd.select().count() + 1
        for gis_hru in gis.Hrus.select():
            id = gis_hru.ID
            hru_name = 'hru%s' % id

            topo = {
                'id': topo_id,
                'name': hru_name,
                'slope': gis_hru.SLOPE / 100,
                'slope_len': 50,
                'lat_len': 50.0,
                'dis_stream': 100.0,
                'dep_co': 1,
                'type': 'hru'
            }
            topos.append(topo)

            hru_data = {
                'name': hru_name,
                'topo': topo_id,
                'hyd': hyd.id,
                'soil': gis_hru.soil_id,
                'land_use_mgt': lum_dict[gis_hru.LANDUSE.lower()],
                'soil_nutr_init': nut.id
            }
            hrus.append(hru_data)
            topo_id += 1

            con = {
                'hru': id,
                'name': hru_name,
                'elev': gis_hru.ELEV,
                'lat': gis_hru.LAT,
                'long': gis_hru.LON,
                'area_ha': gis_hru.ARSLP,
                'overflow': 0,
                'ruleset': 0
            }
            hru_cons.append(con)

            elem_sub = {
                'name': hru_name,
                'ru': gis_hru.SUBBASIN,
                'obtyp': 'hru',
                'obtypno': id,
                'htyp': 'tot',
                'frac': gis_hru.ARSLP / gis_hru.ARSUB
            }
            elem_subs.append(elem_sub)

        db_lib.bulk_insert(self.project_db, hydrology.Topography_hyd, topos)
        db_lib.bulk_insert(self.project_db, hru.Hru_data_hru, hrus)
        db_lib.bulk_insert(self.project_db, connect.Hru_con, hru_cons)
        db_lib.bulk_insert(self.project_db, routing_unit.Rout_unit_ele, elem_subs)
