from peewee import *

db = SqliteDatabase(None)


class BaseModel(Model):
    class Meta:
        database = db


class Layer(BaseModel):
    layer_num = IntegerField()
    z = DoubleField()
    bd = DoubleField()
    awc = DoubleField()
    k = DoubleField()
    cbn = DoubleField()
    clay = DoubleField()
    silt = DoubleField()
    sand = DoubleField()
    rock = DoubleField()
    alb = DoubleField()
    usle_k = DoubleField()
    ec = DoubleField()
    cal = DoubleField(null=True)
    ph = DoubleField(null=True)


class Soil(BaseModel):
    name = CharField()
    muid = CharField(null=True)
    seqn = IntegerField(null=True)
    s5id = CharField(null=True)
    cmppct = IntegerField(null=True)
    hydgrp = CharField()
    zmx = DoubleField()
    anion_excl = DoubleField()
    crk = DoubleField()
    texture = CharField()


class Soil_layer(Layer):
    soil = ForeignKeyField(Soil, on_delete = 'CASCADE')


class Subbasins(BaseModel):
    ID = PrimaryKeyField()
    DRAINCAT = CharField()
    DRAIN = IntegerField()
    AREA = DoubleField()
    SLO1 = DoubleField()
    LEN1 = DoubleField()
    SLL = DoubleField()
    CSL = DoubleField()
    WID1 = DoubleField()
    DEP1 = DoubleField()
    LAT = DoubleField()
    LON = DoubleField()
    ELEV = DoubleField()
    ELEVMIN = DoubleField()
    ELEVMAX = DoubleField()


class Reaches(BaseModel):
    ID = PrimaryKeyField()
    DRAINCAT = CharField()
    DRAIN = IntegerField()
    AREAC = DoubleField()
    LEN2 = DoubleField()
    SLO2 = DoubleField()
    WID2 = DoubleField()
    DEP2 = DoubleField()
    ELEVMIN = DoubleField()
    ELEVMAX = DoubleField()


class Points(BaseModel):
    ID = PrimaryKeyField()
    SUBBASIN = IntegerField()
    LSU = IntegerField()
    DRAINCAT = CharField()
    DRAIN = IntegerField()
    PTYPE = CharField()
    XPR = DoubleField()
    YPR = DoubleField()
    LAT = DoubleField()
    LON = DoubleField()
    ELEV = DoubleField()


class Hrus(BaseModel):
    ID = PrimaryKeyField()
    SUBBASIN = IntegerField()
    LSU = IntegerField()
    RELID = IntegerField()
    DRAINCAT = CharField()
    DRAIN = IntegerField()
    ARSUB = DoubleField()
    ARLSU = DoubleField()
    LANDUSE = CharField()
    ARLAND = DoubleField()
    SOIL = CharField()
    soil_id = IntegerField()
    ARSO = DoubleField()
    SLP = CharField()
    ARSLP = DoubleField()
    SLOPE = DoubleField()
    LAT = DoubleField()
    LON = DoubleField()
    ELEV = DoubleField()


class Lsus(BaseModel):
    ID = PrimaryKeyField()
    LSU = IntegerField()
    SUBBASIN = IntegerField()
    DRAINCAT = CharField()
    DRAIN = IntegerField()
    AREA = DoubleField()
    SLOPE = DoubleField()
    LAT = DoubleField()
    LON = DoubleField()
    ELEV = DoubleField()


class Project(BaseModel):
    name = CharField()              # Project name
    soils_table = CharField()       # Name of soils table in database
    soils_db = CharField()          # Name of soils database file (full path if diff from current dir)
    gis_type = CharField()          # QSWAT+ or ArcSWAT+
    gis_version = CharField()       # Version ID of QSWAT+ or ArcSWAT+
    has_model_run = BooleanField()  # Flag if the user has run SWAT or not
