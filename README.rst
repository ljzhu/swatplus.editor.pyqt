README
======

.. note:: This is in the very early stages of development and not recommended for use at this time. Code is still being planned, and organization and optimization are lacking.

Python interface to SWAT+ allowing the user to import a project from GIS, modify SWAT+ input, write the text files, and run the model.

Read the docs at `swatpluseditor.readthedocs.io <http://swatpluseditor.readthedocs.io>`_
