import os, os.path
import fileinput
import time
import datetime
from timeit import default_timer as timer
import sqlite3
from peewee import *

import traceback
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

from editor.utils import WorkerProgress
from database.project import base as project_base
from database.project.config import Project_config
from database.project.climate import Weather_file, Weather_sta_cli, Weather_wgn_cli, Weather_wgn_cli_mon
from database.project.connect import Aquifer_con, Channel_con, Rout_unit_con, Reservoir_con, Recall_con, Hru_con
from database import lib as db_lib
import math

SWATPLUS_DIR = "swatplus"
HMD_TXT = "hmd.txt"
PCP_TXT = "pcp.txt"
SLR_TXT = "slr.txt"
TMP_TXT = "tmp.txt"
WND_TXT = "wnd.txt"

HMD_CLI = "hmd.cli"
PCP_CLI = "pcp.cli"
SLR_CLI = "slr.cli"
TMP_CLI = "tmp.cli"
WND_CLI = "wnd.cli"

WEATHER_DESC = {
    "hmd": "Relative humidity",
    "pcp": "Precipitation",
    "slr": "Solar radiation",
    "tmp": "Temperature",
    "wnd": "Wind speed"
}


class ImportSwatPlusWeatherWorker(QObject):
    sig_done = pyqtSignal()
    sig_aborted = pyqtSignal()
    sig_error = pyqtSignal(str)
    sig_station_done = pyqtSignal(object)
    sig_update_prog = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.__abort = False

    @pyqtSlot()
    def work(self):
        try:
            if not self.__abort:
                config = Project_config.get()
                self.add_weather_files(config.weather_data_dir)
                self.create_weather_stations(20, 45)
                self.match_stations(65)

                if not self.__abort:
                    self.sig_done.emit()
                else:
                    self.sig_aborted.emit()
        except Exception:
            self.sig_error.emit(traceback.format_exc())

    def abort(self):
        self.__abort = True

    def set_abort(self, val):
        self.__abort = val

    def match_stations(self, start_prog):
        self.match_stations_table(Aquifer_con, "aquifer connections", start_prog)
        self.match_stations_table(Channel_con, "channel connections", start_prog + 5)
        self.match_stations_table(Rout_unit_con, "routing unit connections", start_prog + 10)
        self.match_stations_table(Reservoir_con, "reservoir connections", start_prog + 15)
        self.match_stations_table(Recall_con, "recall connections", start_prog + 20)
        self.match_stations_table(Hru_con, "hru connections", start_prog + 25)

    def match_stations_table(self, table, name, prog):
        self.sig_update_prog.emit(WorkerProgress(prog, "Adding weather stations to {name}...".format(name=name)))
        for row in table.select():
            """
            See: http://stackoverflow.com/a/7472230
            For explanation of getting the closest lat,long
            """
            fudge = math.pow(math.cos(math.radians(row.lat)), 2)
            cursor = project_base.db.execute_sql("select id from weather_sta_cli order by ((? - lat) * (? - lat) + (? - lon) * (? - lon) * ?) limit 1", (row.lat, row.lat, row.long, row.long, fudge))
            res = cursor.fetchone()
            id = res[0]

            row.wst = id
            row.save()

    def create_weather_stations(self, start_prog, total_prog):  #total_prog is the total progress percentage available for this method
        if self.__abort: return

        delete_query = Weather_sta_cli.delete()
        delete_query.execute()

        stations = []
        cursor = project_base.db.execute_sql("select lat, lon from weather_file group by lat, lon")
        data = cursor.fetchall()
        records = len(data)
        i = 1
        for row in data:
            if self.__abort: return

            lat = row[0]
            lon = row[1]
            name = "w{lat}{lon}".format(lat=round(lat*1000), lon=round(lon*1000))

            prog = round(i * total_prog / records) + start_prog
            self.sig_update_prog.emit(WorkerProgress(prog, "Creating weather station {name}...".format(name=name)))

            station = {
                "name": name,
                "wndir": "sim",
                "atmodep": "sim",
                "lat": lat,
                "lon": lon
            }

            for matching_file in Weather_file.select().where((Weather_file.lat == lat) & (Weather_file.lon == lon)):
                if matching_file.type == "hmd":
                    station["hgage"] = matching_file.filename
                elif matching_file.type == "pcp":
                    station["pgage"] = matching_file.filename
                elif matching_file.type == "slr":
                    station["sgage"] = matching_file.filename
                elif matching_file.type == "tmp":
                    station["tgage"] = matching_file.filename
                elif matching_file.type == "wnd":
                    station["wgage"] = matching_file.filename

            stations.append(station)
            i += 1

        db_lib.bulk_insert(project_base.db, Weather_sta_cli, stations)

    def add_weather_files(self, dir):
        delete_query = Weather_file.delete()
        delete_query.execute()

        if self.__abort: return
        hmd_res = self.add_weather_files_type(os.path.join(dir, HMD_CLI), "hmd", 0)
        if self.__abort: return
        pcp_res = self.add_weather_files_type(os.path.join(dir, PCP_CLI), "pcp", 5)
        if self.__abort: return
        slr_res = self.add_weather_files_type(os.path.join(dir, SLR_CLI), "slr", 10)
        if self.__abort: return
        tmp_res = self.add_weather_files_type(os.path.join(dir, TMP_CLI), "tmp", 15)
        if self.__abort: return
        wnd_res = self.add_weather_files_type(os.path.join(dir, WND_CLI), "wnd", 20)

        if self.__abort: return
        warnings = []
        warnings.append(hmd_res)
        warnings.append(pcp_res)
        warnings.append(slr_res)
        warnings.append(tmp_res)
        warnings.append(wnd_res)
        has_warnings = any(x is not None for x in warnings)

        if has_warnings:
            with open(os.path.join( dir, "__warnings.txt"), 'w+') as warning_file:
                for w in warnings:
                    if w is not None:
                        warning_file.write(w)
                        warning_file.write("\n")

    def add_weather_files_type(self, source_file, weather_type, prog):
        if not os.path.exists(source_file):
            return "Skipping {type} import. File does not exist: {file}".format(type=weather_type, file=source_file)
        else:
            self.sig_update_prog.emit(WorkerProgress(prog, "Inserting {type} files and coordinates...".format(type=weather_type)))
            weather_files = []
            dir = os.path.dirname(source_file)
            with open(source_file, "r") as source_data:
                i = 0
                for line in source_data:
                    if self.__abort:
                        break

                    if i > 1:
                        station_name = line.strip('\n')
                        station_file = os.path.join(dir, station_name)
                        if not os.path.exists(station_file):
                            raise IOError("File {file} not found. Weather data import aborted.".format(file=station_file))

                        with open(station_file, "r") as station_data:
                            j = 0
                            for sline in station_data:
                                if j == 2:
                                    station_info = sline.strip().split()
                                    if len(station_info) < 4:
                                        raise ValueError("Invalid value at line {ln} of {file}. Expecting nbyr, tstep, lat, long, elev values separated by a space.".format(ln=str(j + 1), file=station_file))

                                    lat = float(station_info[2])
                                    lon = float(station_info[3])

                                    file = {
                                        "filename": station_name,
                                        "type": weather_type,
                                        "lat": lat,
                                        "lon": lon
                                    }
                                    weather_files.append(file)
                                elif j > 2:
                                    break

                                j += 1

                    i += 1

            db_lib.bulk_insert(project_base.db, Weather_file, weather_files)


class ImportSwat2012WeatherWorker(QObject):
    sig_done = pyqtSignal()
    sig_aborted = pyqtSignal()
    sig_error = pyqtSignal(str)
    sig_station_done = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.__abort = False

    @pyqtSlot()
    def work(self):
        try:
            if not self.__abort:
                config = Project_config.get()
                self.write_to_swatplus(config.weather_data_dir)

                if not self.__abort:
                    self.sig_done.emit()
                else:
                    self.sig_aborted.emit()
        except Exception:
            self.sig_error.emit(traceback.format_exc())

    def abort(self):
        self.__abort = True

    def set_abort(self, val):
        self.__abort = val

    def write_to_swatplus(self, dir):
        # start = timer()
        warnings = []

        output_dir = os.path.join(dir, SWATPLUS_DIR)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        total_files = len(os.listdir(dir))

        if self.__abort: return
        hmd_res = self.write_weather(os.path.join(dir, HMD_TXT), os.path.join(output_dir, HMD_CLI), "hmd", 1, total_files)
        if self.__abort: return
        pcp_res = self.write_weather(os.path.join(dir, PCP_TXT), os.path.join(output_dir, PCP_CLI), "pcp", hmd_res[0], total_files)
        if self.__abort: return
        slr_res = self.write_weather(os.path.join(dir, SLR_TXT), os.path.join(output_dir, SLR_CLI), "slr", pcp_res[0], total_files)
        if self.__abort: return
        tmp_res = self.write_weather(os.path.join(dir, TMP_TXT), os.path.join(output_dir, TMP_CLI), "tmp", slr_res[0], total_files)
        if self.__abort: return
        wnd_res = self.write_weather(os.path.join(dir, WND_TXT), os.path.join(output_dir, WND_CLI), "wnd", tmp_res[0], total_files)

        if self.__abort: return
        warnings.append(hmd_res[1])
        warnings.append(pcp_res[1])
        warnings.append(slr_res[1])
        warnings.append(tmp_res[1])
        warnings.append(wnd_res[1])
        has_warnings = any(x is not None for x in warnings)

        # end = timer()
        # print("Wrote weather data in {sec} seconds".format(sec=(end - start)))

        if has_warnings:
            with open(os.path.join(output_dir, "__warnings.txt"), 'w+') as warning_file:
                for w in warnings:
                    if w is not None:
                        warning_file.write(w)
                        warning_file.write("\n")

        # update config table to use new dir and SWAT+ format
        saved = Project_config.update_weather_data(output_dir, "SWAT+ text files")

    def write_weather(self, source_file, dest_file, weather_type, starting_file_num, total_files):
        if not os.path.exists(source_file):
            return starting_file_num, "Skipping {type} import. File does not exist: {file}".format(type=weather_type, file=source_file)
        else:
            with open(dest_file, 'w+') as new_file:
                new_file.write("{file}.cli: {desc} file names - file written by SWAT+ editor {today}\n".format(file=weather_type, desc=WEATHER_DESC[weather_type], today=datetime.datetime.now()))
                new_file.write("FILENAME\n")

                with open(source_file, "r") as source_data:
                    i = 0
                    curr_file_num = starting_file_num
                    for line in source_data:
                        if self.__abort:
                            break

                        if i == 0 and not "ID,NAME,LAT,LONG,ELEVATION" in line:
                            return curr_file_num, "Skipping {type} import. Invalid file format in header: {file}. Expecting 'ID,NAME,LAT,LONG,ELEVATION'".format(type=weather_type, file=source_file)
                        if i > 0:
                            station_obj = [x.strip() for x in line.split(',')]
                            if len(station_obj) != 5:
                                return curr_file_num, "Skipping {type} import. Invalid file format in line {line_no}: {file}, {line}".format(type=weather_type, line_no=i+1, file=source_file, line=line)

                            new_file_name = "{s}.{ext}".format(s=station_obj[1], ext=weather_type)
                            new_file.write(new_file_name)
                            new_file.write("\n")

                            self.write_station(os.path.dirname(source_file), station_obj, weather_type)
                            prog = round(curr_file_num * 100 / total_files)
                            self.sig_station_done.emit(WorkerProgress(prog, "Writing {type}, {file}...".format(type=weather_type, file=new_file_name)))
                            curr_file_num += 1

                        i += 1

            return curr_file_num, None

    def write_station(self, dir, station_obj, weather_type):
        source_file = os.path.join(dir, "{s}.txt".format(s=station_obj[1]))
        if not os.path.exists(source_file):
            return "Skipping {type} import. Station file does not exist: {file}".format(type=weather_type, file=source_file)

        dest_file_name = "{s}.{ext}".format(s=station_obj[1], ext=weather_type)
        dest_file = os.path.join(dir, SWATPLUS_DIR, dest_file_name)

        nbyr = 0  # need to caluclate this at the end, then edit the value in the file
        place_holder = "<ny>"

        with open(dest_file, 'w+') as new_file:
            new_file.write("{file}: {desc} data - file written by SWAT+ editor {today}\n".format(file=dest_file_name, desc=WEATHER_DESC[weather_type], today=datetime.datetime.now()))
            new_file.write("NBYR".rjust(4))
            new_file.write("TSTEP".rjust(10))
            new_file.write("LAT".rjust(10))
            new_file.write("LONG".rjust(10))
            new_file.write("ELEV".rjust(10))
            new_file.write("\n")

            linecount = self.file_len(source_file)
            total_days = linecount - 1 if linecount > 0 else 0

            with open(source_file, "r") as station_file:
                i = 0
                date = None
                start_date = None
                for line in station_file:
                    if i == 0:
                        ts = time.strptime(line.strip(), "%Y%m%d")
                        date = datetime.datetime(ts.tm_year, ts.tm_mon, ts.tm_mday)
                        start_date = date

                        end_date = start_date + datetime.timedelta(days=total_days)
                        nbyr = end_date.year - start_date.year + 1

                        new_file.write(str(nbyr).rjust(4))
                        new_file.write("0".rjust(10))
                        new_file.write("{0:.3f}".format(float(station_obj[2])).rjust(10))
                        new_file.write("{0:.3f}".format(float(station_obj[3])).rjust(10))
                        new_file.write("{0:.3f}".format(float(station_obj[4])).rjust(10))
                        new_file.write("\n")
                    else:
                        day_of_year = date.timetuple().tm_yday

                        new_file.write(str(date.year))
                        new_file.write(str(day_of_year).rjust(5))

                        if weather_type == "tmp":
                            tmp = [x.strip() for x in line.split(',')]
                            new_file.write(tmp[0].rjust(10))
                            new_file.write(tmp[1].rjust(10))
                            new_file.write("\n")
                        else:
                            new_file.write(line.rjust(10))

                        date = date + datetime.timedelta(days=1)

                    i += 1

                #nbyr = date.year - start_date.year + 1

        # add calculated nbyr to top of file
        """start = timer()
        with fileinput.FileInput(dest_file, inplace=True) as fi:
            for line in fi:
                print(line.replace(place_holder, str(nbyr).rjust(4)), end='')
        end = timer()"""

        # print("Rewrote file in {sec} seconds".format(sec=(end - start)))

    def file_len(self, fname):
        with open(fname) as f:
            for i, l in enumerate(f):
                pass
        return i + 1


class ImportWgnWorker(QObject):
    sig_done = pyqtSignal()
    sig_aborted = pyqtSignal()
    sig_error = pyqtSignal(str)
    sig_update_prog = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.__abort = False

    @pyqtSlot()
    def work(self):
        try:
            if not self.__abort:
                config = Project_config.get()
                self.wgn_database = config.wgn_db
                self.wgn_table = config.wgn_table_name

                self.add_wgn_stations(0, 70)
                self.match_to_weather_stations(70, 30)

                if not self.__abort:
                    self.sig_done.emit()
                else:
                    self.sig_aborted.emit()
        except Exception:
            self.sig_error.emit(traceback.format_exc())

    def add_wgn_stations(self, start_prog, total_prog):
        if self.__abort: return

        delete_query = Weather_wgn_cli.delete()
        delete_query.execute()

        coords = Rout_unit_con.select(fn.Min(Rout_unit_con.lat).alias("min_lat"),
                                      fn.Max(Rout_unit_con.lat).alias("max_lat"),
                                      fn.Min(Rout_unit_con.lon).alias("min_lon"),
                                      fn.Max(Rout_unit_con.lon).alias("max_lon")
                                      ).get()
        print(coords)

        wgns = []
        ids = []
        monthly_values = []

        conn = sqlite3.connect(self.wgn_database)
        conn.row_factory = sqlite3.Row

        monthly_table = "{}_mon".format(self.wgn_table)

        if not db_lib.exists_table(conn, self.wgn_table):
            raise ValueError("Table {table} does not exist in {file}.".format(table=self.wgn_table, file=self.wgn_database))

        if not db_lib.exists_table(conn, monthly_table):
            raise ValueError("Table {table} does not exist in {file}.".format(table=monthly_table, file=self.wgn_database))

        query = "select * from {table_name} where lat between ? and ? and lon between ? and ? order by name".format(table_name=self.wgn_table)
        cursor = conn.cursor().execute(query, (coords.min_lat, coords.max_lat, coords.min_lon, coords.max_lon))
        data = cursor.fetchall()
        records = len(data)

        i = 1
        for row in data:
            if self.__abort: return

            prog = round(i * (total_prog / 2) / records) + start_prog
            self.sig_update_prog.emit(WorkerProgress(prog, "Adding wgn station {name}...".format(name=row['name'])))
            i += 1

            ids.append(row['id'])
            wgn = {
                "id": row['id'],
                "name": row['name'],
                "lat": row['lat'],
                "lon": row['lon'],
                "elev": row['elev'],
                "rain_yrs": row['rain_yrs']
            }
            wgns.append(wgn)

        # Chunk the id array so we don't hit the SQLite parameter limit!
        max_length = 999
        id_chunks = [ids[i:i + max_length] for i in range(0, len(ids), max_length)]

        i = 1
        start_prog = start_prog + (total_prog / 2)

        for chunk in id_chunks:
            mon_query = "select * from {table_name} where wgn_id in ({ids})".format(table_name=monthly_table, ids=",".join('?'*len(chunk)))
            mon_cursor = conn.cursor().execute(mon_query, chunk)
            mon_data = mon_cursor.fetchall()
            mon_records = len(mon_data)

            for row in mon_data:
                if self.__abort: return

                if i == 1 or (i % 12 == 0):
                    prog = round(i * (total_prog / 2) / mon_records) + start_prog
                    self.sig_update_prog.emit(
                        WorkerProgress(prog, "Adding wgn station monthly values {i}/{total}...".format(i=i, total=mon_records)))
                i += 1

                mon = {
                    "id": row['id'],
                    "weather_wgn_cli": row['wgn_id'],
                    "month": row['month'],
                    "tmp_max_ave": row['tmp_max_ave'],
                    "tmp_min_ave": row['tmp_min_ave'],
                    "tmp_max_sd": row['tmp_max_sd'],
                    "tmp_min_sd": row['tmp_min_sd'],
                    "pcp_ave": row['pcp_ave'],
                    "pcp_sd": row['pcp_sd'],
                    "pcp_skew": row['pcp_skew'],
                    "wet_dry": row['wet_dry'],
                    "wet_wet": row['wet_wet'],
                    "pcp_days": row['pcp_days'],
                    "pcp_hhr": row['pcp_hhr'],
                    "slr_ave": row['slr_ave'],
                    "dew_ave": row['dew_ave'],
                    "wnd_ave": row['wnd_ave']
                }
                monthly_values.append(mon)

        db_lib.bulk_insert(project_base.db, Weather_wgn_cli, wgns)
        db_lib.bulk_insert(project_base.db, Weather_wgn_cli_mon, monthly_values)

    def match_to_weather_stations(self, start_prog, total_prog):
        query = Weather_sta_cli.select()
        records = query.count()
        i = 1
        for row in query:
            if self.__abort: return

            prog = round(i * total_prog / records) + start_prog
            self.sig_update_prog.emit(
                WorkerProgress(prog, "Updating weather station with wgn station {i}/{total}...".format(i=i, total=records)))
            i += 1
            """
            See: http://stackoverflow.com/a/7472230
            For explanation of getting the closest lat,long
            """
            fudge = math.pow(math.cos(math.radians(row.lat)), 2)
            cursor = project_base.db.execute_sql("select id from weather_wgn_cli order by ((? - lat) * (? - lat) + (? - lon) * (? - lon) * ?) limit 1", (row.lat, row.lat, row.lon, row.lon, fudge))
            res = cursor.fetchone()
            id = res[0]

            row.weather_wgn_cli = id
            row.save()

    def abort(self):
        self.__abort = True

    def set_abort(self, val):
        self.__abort = val
