import os.path
import traceback
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot

from editor.utils import WorkerProgress

from database.project.config import Project_config
from database.project.config import File_cio as project_file_cio
from database.datasets.definitions import File_cio as default_file_cio

from fileio.connect import Hru_con, Rout_unit_con, Aquifer_con, Channel_con, Reservoir_con, Recall_con, Recall_rec
from fileio.climate import Weather_sta_cli, Weather_wgn_cli
from fileio.channel import Channel_cha, Hydrology_cha, Initial_cha, Sediment_cha, Nutrients_cha, Pesticide_cha
from fileio.aquifer import Aquifer_aqu
from fileio.hydrology import Hydrology_hyd, Topography_hyd
from fileio.reservoir import Reservoir_res
from fileio.hru import Hru_data_hru
from fileio.lum import Landuse_lum
from fileio.soils import Nutrients_sol, Soils_sol
from fileio.init import Initial_plt
from fileio.routing_unit import Rout_unit, Rout_unit_ele

NULL_FILE = "null"


class WriteInputFilesWorker(QObject):
    sig_done = pyqtSignal()
    sig_aborted = pyqtSignal()
    sig_error = pyqtSignal(str)
    sig_update_prog = pyqtSignal(object)

    def __init__(self):
        super().__init__()
        self.__abort = False

    @pyqtSlot()
    def work(self):
        try:
            if not self.__abort:
                config = Project_config.get()
                self.__dir = config.input_files_dir
                self.__version = config.editor_version
                self.__current_progress = 0
                
                #self.write_connect(0, 20)
                self.write_climate(20, 20)
                #self.write_channel(40, 5)
                #self.write_aquifer(45, 5)
                #self.write_hydrology(50, 5)
                #self.write_reservoir(55, 5)
                #self.write_hru(60, 10)
                #self.write_lum(70, 5)
                #self.write_soils(75, 10)
                #self.write_init(85, 5)
                #self.write_routing_unit(90, 5)

                if not self.__abort:
                    self.sig_done.emit()
                else:
                    self.sig_aborted.emit()
        except Exception:
            self.sig_error.emit(traceback.format_exc())

    def abort(self):
        self.__abort = True

    def set_abort(self, val):
        self.__abort = val

    def get_file_names(self, section, num_required):
        connect_files = []

        try:
            row = project_file_cio.get(project_file_cio.section == section)
            connect_files = row.file_names.split(',')
        except project_file_cio.DoesNotExist:
            row = default_file_cio.get(default_file_cio.section == section)
            connect_files = row.default_file_names.split(',')

        if len(connect_files) < num_required:
            raise ValueError(
                "{section} file names not available in the project database nor the SWAT+ datasets database.".format(section=section))

        return connect_files

    def write_connect(self, start_prog, allocated_prog):
        num_files = 13
        files = self.get_file_names("connect", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        hru_con_file = files[0].strip()
        if hru_con_file != NULL_FILE:
            self.update_file_status(prog, hru_con_file)
            Hru_con(os.path.join(self.__dir, hru_con_file), self.__version).write()

        prog += prog_step
        rout_unit_con_file = files[2].strip()
        if rout_unit_con_file != NULL_FILE:
            self.update_file_status(prog, rout_unit_con_file)
            Rout_unit_con(os.path.join(self.__dir, rout_unit_con_file), self.__version).write()

        prog += prog_step
        aquifer_con_file = files[4].strip()
        if aquifer_con_file != NULL_FILE:
            self.update_file_status(prog, aquifer_con_file)
            Aquifer_con(os.path.join(self.__dir, aquifer_con_file), self.__version).write()

        prog += prog_step
        channel_con_file = files[6].strip()
        if channel_con_file != NULL_FILE:
            self.update_file_status(prog, channel_con_file)
            Channel_con(os.path.join(self.__dir, channel_con_file), self.__version).write()

        prog += prog_step
        reservoir_con_file = files[7].strip()
        if reservoir_con_file != NULL_FILE:
            self.update_file_status(prog, reservoir_con_file)
            Reservoir_con(os.path.join(self.__dir, reservoir_con_file), self.__version).write()

        prog += prog_step
        recall_con_file = files[8].strip()
        if recall_con_file != NULL_FILE:
            self.update_file_status(prog, recall_con_file)
            Recall_con(os.path.join(self.__dir, recall_con_file), self.__version).write()
            Recall_rec(os.path.join(self.__dir, "recall.rec"), self.__version).write()

    def write_climate(self, start_prog, allocated_prog):
        num_files = 8
        files = self.get_file_names("climate", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        weather_sta_file = files[0].strip()
        if weather_sta_file != NULL_FILE:
            self.update_file_status(prog, weather_sta_file)
            Weather_sta_cli(os.path.join(self.__dir, weather_sta_file), self.__version).write()

        prog += prog_step
        weather_wgn_file = files[1].strip()
        if weather_wgn_file != NULL_FILE:
            self.update_file_status(prog, weather_wgn_file)
            Weather_wgn_cli(os.path.join(self.__dir, weather_wgn_file), self.__version).write()

    def write_channel(self, start_prog, allocated_prog):
        num_files = 7
        files = self.get_file_names("channel", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        channel_cha_file = files[1].strip()
        if channel_cha_file != NULL_FILE:
            self.update_file_status(start_prog, channel_cha_file)
            Channel_cha(os.path.join(self.__dir, channel_cha_file), self.__version).write()

        prog += prog_step
        initial_cha_file = files[2].strip()
        if initial_cha_file != NULL_FILE:
            self.update_file_status(prog, initial_cha_file)
            Initial_cha(os.path.join(self.__dir, initial_cha_file), self.__version).write()

        prog += prog_step
        hydrology_cha_file = files[2].strip()
        if hydrology_cha_file != NULL_FILE:
            self.update_file_status(prog, hydrology_cha_file)
            Hydrology_cha(os.path.join(self.__dir, hydrology_cha_file), self.__version).write()

        prog += prog_step
        sediment_cha_file = files[2].strip()
        if sediment_cha_file != NULL_FILE:
            self.update_file_status(prog, sediment_cha_file)
            Sediment_cha(os.path.join(self.__dir, sediment_cha_file), self.__version).write()

        prog += prog_step
        nutrients_cha_file = files[2].strip()
        if nutrients_cha_file != NULL_FILE:
            self.update_file_status(prog, nutrients_cha_file)
            Nutrients_cha(os.path.join(self.__dir, nutrients_cha_file), self.__version).write()

        prog += prog_step
        pesticide_cha_file = files[2].strip()
        if pesticide_cha_file != NULL_FILE:
            self.update_file_status(prog, pesticide_cha_file)
            Pesticide_cha(os.path.join(self.__dir, pesticide_cha_file), self.__version).write()

    def write_aquifer(self, start_prog, allocated_prog):
        num_files = 1
        files = self.get_file_names("aquifer", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        aquifer_aqu_file = files[0].strip()
        if aquifer_aqu_file != NULL_FILE:
            self.update_file_status(start_prog, aquifer_aqu_file)
            Aquifer_aqu(os.path.join(self.__dir, aquifer_aqu_file), self.__version).write()

    def write_hydrology(self, start_prog, allocated_prog):
        num_files = 3
        files = self.get_file_names("hydrology", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        hydrology_hyd_file = files[0].strip()
        if hydrology_hyd_file != NULL_FILE:
            self.update_file_status(start_prog, hydrology_hyd_file)
            Hydrology_hyd(os.path.join(self.__dir, hydrology_hyd_file), self.__version).write()

        prog += prog_step
        topography_hyd_file = files[1].strip()
        if topography_hyd_file != NULL_FILE:
            self.update_file_status(start_prog + 5, topography_hyd_file)
            Topography_hyd(os.path.join(self.__dir, topography_hyd_file), self.__version).write()

    def write_reservoir(self, start_prog, allocated_prog):
        num_files = 7
        files = self.get_file_names("reservoir", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        reservoir_res_file = files[1].strip()
        if reservoir_res_file != NULL_FILE:
            self.update_file_status(start_prog, reservoir_res_file)
            Reservoir_res(os.path.join(self.__dir, reservoir_res_file), self.__version).write()

    def write_hru(self, start_prog, allocated_prog):
        num_files = 1
        files = self.get_file_names("hru", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        hru_data_file = files[0].strip()
        if hru_data_file != NULL_FILE:
            self.update_file_status(start_prog, hru_data_file)
            Hru_data_hru(os.path.join(self.__dir, hru_data_file), self.__version).write()

    def write_lum(self, start_prog, allocated_prog):
        num_files = 5
        files = self.get_file_names("lum", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        landuse_lum_file = files[0].strip()
        if landuse_lum_file != NULL_FILE:
            self.update_file_status(start_prog, landuse_lum_file)
            Landuse_lum(os.path.join(self.__dir, landuse_lum_file), self.__version).write()

    def write_soils(self, start_prog, allocated_prog):
        num_files = 2
        files = self.get_file_names("soils", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        nutrients_sol_file = files[1].strip()
        if nutrients_sol_file != NULL_FILE:
            self.update_file_status(start_prog, nutrients_sol_file)
            Nutrients_sol(os.path.join(self.__dir, nutrients_sol_file), self.__version).write()

        prog += prog_step
        soils_sol_file = files[0].strip()
        if soils_sol_file != NULL_FILE:
            self.update_file_status(start_prog + 5, soils_sol_file)
            Soils_sol(os.path.join(self.__dir, soils_sol_file), self.__version).write()

    def write_init(self, start_prog, allocated_prog):
        num_files = 2
        files = self.get_file_names("init", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        initial_plt_file = files[1].strip()
        if initial_plt_file != NULL_FILE:
            self.update_file_status(start_prog, initial_plt_file)
            Initial_plt(os.path.join(self.__dir, initial_plt_file), self.__version).write()

    def write_routing_unit(self, start_prog, allocated_prog):
        num_files = 4
        files = self.get_file_names("routing_unit", num_files)

        prog_step = round(allocated_prog / num_files)
        prog = start_prog

        rout_unit_ru_file = files[2].strip()
        rout_unit_def_file = files[0].strip()
        if rout_unit_ru_file != NULL_FILE:
            self.update_file_status(start_prog, rout_unit_ru_file)
            Rout_unit(os.path.join(self.__dir, rout_unit_ru_file), os.path.join(self.__dir, rout_unit_def_file), self.__version).write()

        prog += prog_step
        rout_unit_ele_file = files[1].strip()
        if rout_unit_ele_file != NULL_FILE:
            self.update_file_status(start_prog, rout_unit_ele_file)
            Rout_unit_ele(os.path.join(self.__dir, rout_unit_ele_file), self.__version).write()

    def update_file_status(self, prog, file_name):
        self.sig_update_prog.emit(WorkerProgress(prog, "Writing {name}...".format(name=file_name)))
