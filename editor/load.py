import time
import traceback
from PyQt5.QtCore import QObject, pyqtSignal, pyqtSlot
from peewee import *

from editor.config import SWATEditorConfig
#from database.gis_to_project import GisToProject
from database.project.simulation import Time_sim
from database.project.config import Project_config

from database.project import base as project_base
from database.datasets import base as datasets_base

from database.project import routing_unit
from database.project.setup import SetupProjectDatabase


class LoadProjectWorker(QObject):
    sig_build = pyqtSignal(object)  # Create databases if necessary
    sig_import_gis = pyqtSignal()
    sig_sim = pyqtSignal(object)  # Time_sim object
    sig_done = pyqtSignal()
    sig_error = pyqtSignal(str)

    def __init__(self):
        super().__init__()
        self.__abort = False

    def set_dbs(self, project_db: str, datasets_db: str):
        project_base.db.init(project_db)
        datasets_base.db.init(datasets_db)

        self.project_db_name = project_db
        self.datasets_db_name = datasets_db

    @pyqtSlot()
    def work(self):
        try:
            if not self.__abort:
                SetupProjectDatabase.create_tables()
                SetupProjectDatabase.copy_default_datasets()
                config = Project_config.get_or_create_default(new_version=SWATEditorConfig.VERSION)
                self.sig_build.emit(config)

                """if self.gis_db_name != '' and routing_unit.Rout_unit_rtu.select().count() < 1:
                    GisToProject(self.project_db_name, self.gis_db_name, self.datasets_db_name).insert_default()
                    self.sig_import_gis.emit()"""

                time_sim = Time_sim.get_or_create_default()
                self.sig_sim.emit(time_sim)

                time.sleep(0.5)
                self.sig_done.emit()
        except Exception:
            self.sig_error.emit(traceback.format_exc())

    def abort(self):
        self.__abort = True

    def set_abort(self, val):
        self.__abort = val
