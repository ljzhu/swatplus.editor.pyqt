import json
import os.path


class SaveProject:
    @staticmethod
    def config(file, version, name, project_db, datasets_db, model):
        try:
            rel_project_db = os.path.relpath(project_db, os.path.dirname(file))
        except ValueError:
            rel_project_db = project_db

        try:
            rel_datasets_db = os.path.relpath(datasets_db, os.path.dirname(file))
        except ValueError:
            rel_datasets_db = datasets_db

        data = {
            "swatplus-project": {
                "version": version,
                "name": name,
                "databases": {
                    "project": rel_project_db,
                    "datasets": rel_datasets_db
                },
                "model": model
            }
        }

        with open(file, 'w+') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)