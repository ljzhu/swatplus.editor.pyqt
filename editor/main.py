from PyQt5 import QtWidgets, QtGui
from PyQt5.QtCore import QObject, QThread, pyqtSignal, pyqtSlot, QSettings
from enum import Enum
import os
import sys
import mainwindow
import json
import datetime
import operator
import sqlite3
from editor.load import LoadProjectWorker
from editor.weather import ImportSwat2012WeatherWorker, ImportSwatPlusWeatherWorker, ImportWgnWorker
from editor.files import WriteInputFilesWorker
from editor.save import SaveProject
from editor.config import SWATEditorConfig
from editor.utils import WorkerProgress
import utils
from database.project.simulation import Time_sim
from database.project.config import Project_config

version = SWATEditorConfig.VERSION
tech_support_email = SWATEditorConfig.SUPPORT_EMAIL


class LoadProgress(Enum):
    START = 0
    BUILT_DB = 10
    IMPORTED_GIS = 20
    LOADED_SIM = 25
    DONE = 100

    @classmethod
    def message(cls, val):
        msgs = {
            cls.START: 'Loading SWAT+ project config and building database...',
            cls.BUILT_DB: 'Importing GIS data if applicable...',
            cls.IMPORTED_GIS: 'Loading simulation data...',
            cls.LOADED_SIM: 'Done loading simulation...',
            cls.DONE: 'Your project is done loading.'
        }
        return msgs[val]


class RecentProject:
    def __init__(self, date_loaded, name, file_path):
        self.date_loaded = date_loaded
        self.name = name
        self.file_path = file_path


class SWATPlusEditor(QtWidgets.QMainWindow, mainwindow.Ui_MainWindow):
    sig_abort_workers = pyqtSignal()
    #sig_abort_import_swat2012_weather = pyqtSignal()

    # <editor-fold desc="Init">
    def __init__(self, parent=None):
        super(SWATPlusEditor, self).__init__(parent)
        self.setupUi(self)
        self.toggle_action(self.actionSetup, 0)

        # Load user settings
        self.__settings = QSettings("tamu", "swatplus_editor")
        self.__recent_projects = self.__settings.value("recent_projects", [])
        self.connect_recent_projects()
        self.recentProjectsList.itemClicked.connect(self.recent_project_clicked)

        self.__last_used_path = self.__settings.value("last_used_path", str)
        if self.__last_used_path == '':
            self.__last_used_path = os.path.expanduser('~')

        # Connect main toolbar
        self.actionExit.triggered.connect(QtWidgets.qApp.quit)
        self.mainToolBar.actionTriggered[QtWidgets.QAction].connect(self.main_toolbar_btn_pressed)

        # Initialize sections
        self.reset_status_messages()
        self.connect_project_setup()
        self.connect_load_project_worker()
        self.connect_import_wgn_worker()
        self.connect_import_swat2012_weather_worker()
        self.connect_import_swatplus_weather_worker()
        self.connect_write_files_worker()

        self.connect_simulation()
        self.connect_climate()
        self.connect_write_files()

        self.__project_is_loaded = False
    # </editor-fold>

    # <editor-fold desc="MainToolbar">
    def main_toolbar_btn_pressed(self, a):
        self.stackedWidget.setCurrentIndex(self.set_stacked_widget_index(a.text()))
        self.set_main_toolbar_checked()

    def set_main_toolbar_checked(self):
        self.toggle_action(self.actionSetup, 0)
        self.toggle_action(self.actionSimulation, 1)
        self.toggle_action(self.actionClimate, 2)
        self.toggle_action(self.actionWrite_Input_Files, 3)
        self.toggle_action(self.actionRun_SWAT, 0, True)
        self.toggle_action(self.actionHelp, 0, True)
        self.toggle_action(self.actionConnections, 0, True)
        self.toggle_action(self.actionChannel, 0, True)
        self.toggle_action(self.actionReservoir, 0, True)
        self.toggle_action(self.actionSubbasin, 0, True)
        self.toggle_action(self.actionAquifer, 0, True)
        self.toggle_action(self.actionHRUs, 0, True)
        self.toggle_action(self.actionManagement, 0, True)
        self.toggle_action(self.actionDatabases, 0, True)

    def toggle_action(self, action, index, override=False):
        action.setChecked(False if override else self.is_stacked_widget_at_index(index))

    def is_stacked_widget_at_index(self, index):
        return True if self.stackedWidget.currentIndex() == index else False

    def set_stacked_widget_index(self, x):
        return {
            'Project Setup': 0,
            'Simulation': 1,
            'Climate': 2,
            'Write Files': 3,
            'Run SWAT +': 0,
            'Help': 0
        }.get(x, 0)
    # </editor-fold>

    # <editor-fold desc="SectionInitializers">
    def connect_project_setup(self):
        self.existingProjectSettingsBox.setEnabled(False)

        if len(self.__recent_projects) > 0:
            self.existingProject.setText(self.__recent_projects[0].file_path)
        else:
            self.existingProject.setText(os.path.join(os.getcwd(), "data", "example-project", "swatplus_project_settings.json"))

        self.datasetsDatabase.setText(os.path.join(os.getcwd(), "data", "example-project", "swatplus_datasets.sqlite"))

        self.existingProjectBrowseBtn.clicked.connect(self.open_existing_project)
        self.existingProjectDatabaseBrowseBtn.clicked.connect(self.open_existing_project_database)
        self.existingDatasetsDatabaseBrowseBtn.clicked.connect(self.open_existing_datasets_database)
        self.projectDatabaseBrowseBtn.clicked.connect(self.open_project_database)
        self.datasetsDatabaseBrowseBtn.clicked.connect(self.open_datasets_database)
        self.loadExistingProject.clicked.connect(self.load_project)
        self.saveExistingProjectSettings.clicked.connect(self.update_project_settings)
        self.saveProjectConfigBtn.clicked.connect(self.save_new_project)

    def connect_simulation(self):
        self.saveTimeSim.clicked.connect(self.save_time_sim)
        self.jd_start.valueChanged.connect(lambda: self.set_unsaved_changes_warning(self.simStatusIcon, self.simStatusLabel))
        self.yr_start.valueChanged.connect(lambda: self.set_unsaved_changes_warning(self.simStatusIcon, self.simStatusLabel))
        self.jd_end.valueChanged.connect(lambda: self.set_unsaved_changes_warning(self.simStatusIcon, self.simStatusLabel))
        self.yr_end.valueChanged.connect(lambda: self.set_unsaved_changes_warning(self.simStatusIcon, self.simStatusLabel))
        self.stepOption.currentIndexChanged.connect(lambda: self.set_unsaved_changes_warning(self.simStatusIcon, self.simStatusLabel))

    def connect_climate(self):
        self.wgnDbBrowser.clicked.connect(self.open_wgn_database)
        self.wgnDb.textChanged.connect(self.load_wgn_tables)
        self.addWgnDbTable.clicked.connect(self.add_wgn_to_project)

        self.weatherStationsBox.setEnabled(False)
        self.weatherDirectoryBrowseBtn.clicked.connect(self.open_weather_directory)
        self.importWeather.clicked.connect(self.import_weather_data)
        self.weatherDirectory.textChanged.connect(lambda: self.set_unsaved_changes_warning(self.weatherStationsStatusIcon, self.weatherStationsStatusLabel))

    def connect_write_files(self):
        self.inputFilesDirBrowseBtn.clicked.connect(self.open_input_files_directory)
        self.writeFiles.clicked.connect(self.write_files)

    def reset_status_messages(self):
        self.simStatusIcon.setVisible(False)
        self.simStatusLabel.setVisible(False)
        self.wgnStatusIcon.setVisible(False)
        self.wgnStatusLabel.setVisible(False)
        self.weatherStationsStatusIcon.setVisible(False)
        self.weatherStationsStatusLabel.setVisible(False)
    # </editor-fold>

    # <editor-fold desc="RecentProjects">
    def connect_recent_projects(self):
        for rpro in self.__recent_projects:
            item = QtWidgets.QListWidgetItem(rpro.name)
            item.setToolTip("{p}, loaded on {d}".format(p=rpro.file_path, d=rpro.date_loaded.strftime("%b %d, %Y %I:%M %p")))
            self.recentProjectsList.addItem(item)

    def recent_project_clicked(self, item):
        rpros = list(filter(lambda x: x.name == item.text(), self.__recent_projects))
        if len(rpros) > 0:
            self.existingProject.setText(rpros[0].file_path)

    def add_to_recent_projects(self, name, file_path):
        matches = list(filter(lambda x: x.file_path == file_path, self.__recent_projects))
        if len(matches) < 1:
            self.__recent_projects.insert(0, RecentProject(datetime.datetime.now(), name, file_path))
            if len(self.__recent_projects) > 5:
                self.__recent_projects.pop()
        else:
            match = matches[0]
            match.name = name
            match.date_loaded = datetime.datetime.now()

            old_index = self.__recent_projects.index(match)
            self.__recent_projects.insert(0, self.__recent_projects.pop(old_index))


        self.__settings.setValue("recent_projects", self.__recent_projects)
        self.recentProjectsList.clear()
        self.connect_recent_projects()
    # </editor-fold>

    # <editor-fold desc="FileDialogs">
    def get_existing_project_path(self):
        path = self.__last_used_path
        if self.__last_used_path is None:
            path = os.path.dirname(self.existingProjectDatabase.text())

        return str(path)

    def set_last_used_path(self, file):
        self.__last_used_path = os.path.dirname(file)
        self.__settings.setValue("last_used_path", self.__last_used_path)

    def open_existing_project(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, self.tr('Open existing SWAT+ settings file'),
                                                            self.get_existing_project_path(), '(*.json);;')
        if fileName:
            self.existingProject.setText(fileName)
            self.set_last_used_path(fileName)

    def open_existing_project_database(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, self.tr('Open SWAT+ project database'),
                                                            self.get_existing_project_path(), '(*.sqlite);;')
        if fileName:
            self.existingProjectDatabase.setText(fileName)
            self.set_last_used_path(fileName)

    def open_existing_datasets_database(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, self.tr('Open SWAT+ datasets database'),
                                                            self.get_existing_project_path(), '(*.sqlite);;')
        if fileName:
            self.existingDatasetsDatabase.setText(fileName)
            self.set_last_used_path(fileName)

    def open_project_database(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, self.tr('Open SWAT+ project database'),
                                                            str(self.__last_used_path), '(*.sqlite);;')
        if fileName:
            self.projectDatabase.setText(fileName)
            self.set_last_used_path(fileName)

    def open_datasets_database(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, self.tr('Open SWAT+ datasets database'),
                                                            str(self.__last_used_path), '(*.sqlite);;')
        if fileName:
            self.datasetsDatabase.setText(fileName)
            self.set_last_used_path(fileName)

    def open_wgn_database(self):
        fileName, _ = QtWidgets.QFileDialog.getOpenFileName(self, self.tr('Open SWAT+ weather generator database'),
                                                            str(self.__last_used_path), '(*.sqlite);;')
        if fileName:
            self.wgnDb.setText(fileName)
            self.set_last_used_path(fileName)

    def open_weather_directory(self):
        dir = QtWidgets.QFileDialog.getExistingDirectory(self, self.tr('Select your weather data directory'), str(self.__last_used_path), QtWidgets.QFileDialog.ShowDirsOnly)

        if dir:
            self.weatherDirectory.setText(dir)
            self.set_last_used_path(dir)

    def open_input_files_directory(self):
        dir = QtWidgets.QFileDialog.getExistingDirectory(self, self.tr('Select your text in/out directory'),
                                                         str(self.__last_used_path), QtWidgets.QFileDialog.ShowDirsOnly)

        if dir:
            self.inputFilesDir.setText(dir)
            self.set_last_used_path(dir)
    # </editor-fold>

    # <editor-fold desc="LoadProjectWorker">
    def connect_load_project_worker(self):
        self.load_project_thread = QThread()
        self.load_project_worker = LoadProjectWorker()
        self.load_project_worker.moveToThread(self.load_project_thread)
        self.load_project_worker.sig_build.connect(self.build_tables)
        self.load_project_worker.sig_import_gis.connect(self.import_gis)
        self.load_project_worker.sig_sim.connect(self.load_simulation)
        self.load_project_worker.sig_done.connect(self.on_load_project_done)
        self.load_project_worker.sig_error.connect(self.show_program_fail_message_box)
        self.sig_abort_workers.connect(self.load_project_worker.abort)
        self.load_project_thread.started.connect(self.load_project_worker.work)

    def start_loading_project(self, project_db, datasets_db):
        self.__project_is_loaded = False
        self.load_project_worker.set_abort(False)
        self.load_project_worker.set_dbs(project_db, datasets_db)
        self.load_project_thread.start()

        self.load_project_progdialog = QtWidgets.QProgressDialog("Loading SWAT+ project...", "Cancel",
                                                                 0, 100, self)
        self.load_project_progdialog.setFixedWidth(350)
        self.load_project_progdialog.setWindowTitle("Load Project")
        self.load_project_progdialog.canceled.connect(self.abort_workers)
        self.load_project_progdialog.setModal(True)

        self.load_project_progdialog.setValue(0)
        self.load_project_progdialog.exec_()

    def set_load_project_progdialog(self, progress):
        self.load_project_progdialog.setLabelText(LoadProgress.message(progress))
        self.load_project_progdialog.setValue(progress.value)

    @pyqtSlot(object)
    def build_tables(self, config):
        self.__project_config = config

        self.weatherDirectory.setText(config.weather_data_dir)
        if config.weather_data_format == "SWAT+ text files":
            self.weatherImportFormat.setCurrentIndex(1)
        elif config.weather_data_format == "SWAT2012 text files":
            self.weatherImportFormat.setCurrentIndex(2)

        self.wgnDb.setText(config.wgn_db)
        index = self.wgnDbTable.findText(config.wgn_table_name)
        if index >= 0:
            self.wgnDbTable.setCurrentIndex(index)

        self.inputFilesDir.setText(config.input_files_dir)
        self.set_load_project_progdialog(LoadProgress.BUILT_DB)

    @pyqtSlot()
    def import_gis(self):
        self.set_load_project_progdialog(LoadProgress.IMPORTED_GIS)

    @pyqtSlot(object)
    def load_simulation(self, time_sim):
        if time_sim is not None:
            self.jd_start.setValue(time_sim.jd_start)
            self.yr_start.setValue(time_sim.yr_start)
            self.jd_end.setValue(time_sim.jd_end)
            self.yr_end.setValue(time_sim.yr_end)
            self.stepOption.setCurrentIndex(self.get_time_sim_step_index(time_sim.step))
            self.set_load_project_progdialog(LoadProgress.LOADED_SIM)

    @pyqtSlot()
    def on_load_project_done(self):
        self.load_project_progdialog.close()
        self.load_project_thread.quit()
        self.load_project_thread.wait()
        self.existingProjectSettingsBox.setEnabled(True)
        self.__project_is_loaded = True
        self.add_to_recent_projects(self.existingProjectName.text(), self.existingProject.text())
        self.projectSetupTabWidget.setCurrentIndex(0)
        self.reset_status_messages()
        self.show_message_box("Project Loaded",
                              "Your project has been loaded. Use the toolbar on the left to start editing.")

    @pyqtSlot()
    def abort_workers(self):
        self.sig_abort_workers.emit()
        self.load_project_progdialog.close()
        self.load_project_thread.quit()
        self.load_project_thread.wait()
    # </editor-fold>

    # <editor-fold desc="ProjectSetup">
    def save_new_project(self):
        error_icon = QtWidgets.QMessageBox.Critical
        error_title = "Error saving new SWAT+ Editor project"
        if self.projectName.text() == "":
            self.show_message_box(error_title, "Please enter a project name.", error_icon)
        elif self.datasetsDatabase.text() == "":
            self.show_message_box(error_title, "Please select a SWAT+ datasets database.", error_icon)
        else:
            name_slug = utils.get_valid_filename(self.projectName.text())
            file_name, _ = QtWidgets.QFileDialog.getSaveFileName(self, self.tr('Open existing SWAT+ settings file'), os.path.join(self.__last_used_path, name_slug), '(*.json);;')

            if file_name:
                project_db = self.projectDatabase.text()

                project_dir = os.path.dirname(file_name)
                if self.projectDatabase.text() == "":
                    project_db = os.path.join(project_dir, "{name}.sqlite".format(name=name_slug))

                model = 'SWAT+' if self.fullModelOption.isChecked() else 'SWAT+ lte'
                SaveProject.config(file_name,
                                   version,
                                   self.projectName.text(),
                                   project_db,
                                   self.datasetsDatabase.text(),
                                   model)

                self.existingProject.setText(file_name)
                self.existingProjectName.setText(self.projectName.text())
                self.existingProjectDatabase.setText(project_db)
                self.existingDatasetsDatabase.setText(self.datasetsDatabase.text())
                if self.fullModelOption.isChecked():
                    self.existingFullModelOption.setChecked(True)
                else:
                    self.existingLteModelOption.setChecked(True)

                self.start_loading_project(project_db, self.datasetsDatabase.text())

    def load_project(self):
        error_icon = QtWidgets.QMessageBox.Critical
        error_title = "Invalid SWAT+ Editor project settings file"
        error = "Invalid SWAT+ Editor project settings file. If you have not used SWAT+ Editor before, set up a new project first by selecting your database files below."

        try:
            with open(self.existingProject.text(), encoding='utf-8') as data_file:
                data = json.load(data_file)
                project_path = os.path.dirname(self.existingProject.text())
                try:
                    if self.check_version_compatibility(data["swatplus-project"]["version"]):
                        self.existingProjectName.setText(data["swatplus-project"]["name"])
                        self.existingProjectDatabase.setText(self.get_full_file_path(data["swatplus-project"]["databases"]["project"], project_path))
                        self.existingDatasetsDatabase.setText(self.get_full_file_path(data["swatplus-project"]["databases"]["datasets"], project_path))
                        model = data["swatplus-project"]["model"]
                        if model == 'SWAT+ lte':
                            self.existingLteModelOption.setChecked(True)
                        else:
                            self.existingFullModelOption.setChecked(True)

                        if self.__project_is_loaded:
                            msg = QtWidgets.QMessageBox()
                            msg.setIcon(QtWidgets.QMessageBox.Warning)
                            msg.setWindowTitle("Close open project?")
                            msg.setText("This action will close the project you're currently working on. Click 'OK' to continue.")
                            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
                            reply = msg.exec_()

                            if reply == QtWidgets.QMessageBox.Ok:
                                self.start_loading_project(self.existingProjectDatabase.text(), self.existingDatasetsDatabase.text())
                        else:
                            self.start_loading_project(self.existingProjectDatabase.text(), self.existingDatasetsDatabase.text())


                    else:
                        self.show_message_box("Incompatible version",
                                                    "Your project version is not compatible with this version of SWAT+ Editor. Check the help docs to update your project.",
                                              error_icon)
                except KeyError:
                    self.show_message_box("Invalid SWAT+ project settings file",
                                                "There was a problem reading your SWAT+ project settings file. "
                                                "Some settings may not have loaded correctly which could cause errors while using the editor. "
                                                "Check the help docs to ensure your file is formatted correctly.",
                                          error_icon)
        except IOError:
            self.show_message_box(error_title, error, error_icon)
        except ValueError:
            self.show_message_box(error_title, error, error_icon)
        except Exception as e:
            self.show_program_fail_message_box(e)

    def update_project_settings(self):
        model = 'SWAT+' if self.existingFullModelOption.isChecked() else 'SWAT+ lte'
        SaveProject.config(self.existingProject.text(),
                           version,
                           self.existingProjectName.text(),
                           self.existingProjectDatabase.text(),
                           self.existingDatasetsDatabase.text(),
                           model)
        self.start_loading_project(self.existingProjectDatabase.text(), self.existingDatasetsDatabase.text())

    def check_version_compatibility(self, version):
        """
        TO DO! Currently returns true always since we're in development.
        :return: True if project version is compatible with current version of SWAT+ Editor
        """
        return True
    # </editor-fold>

    # <editor-fold desc="Simulation">
    def save_time_sim(self):
        result = Time_sim.update_and_exec(self.jd_start.value(), self.yr_start.value(), self.jd_end.value(), self.yr_end.value(), self.get_time_sim_step_value(self.stepOption.currentIndex()))
        if result == 1:
            self.set_saved_changes_success(self.simStatusIcon, self.simStatusLabel, "Simulation")
        else:
            self.set_saved_changes_error(self.simStatusIcon, self.simStatusLabel)

    def get_time_sim_step_index(self, value):
        if value == 1440:
            return 4
        elif value == 96:
            return 3
        elif value == 24:
            return 2
        elif value == 1:
            return 1
        else:
            return 0

    def get_time_sim_step_value(self, index):
        if index == 4:
            return 1440
        elif index == 3:
            return 96
        elif index == 2:
            return 24
        elif index == 1:
            return 1
        else:
            return 0
    # </editor-fold>

    # <editor-fold desc="ImportSwat2012WeatherWorker">
    def connect_import_swat2012_weather_worker(self):
        self.import_swat2012_weather_thread = QThread()
        self.import_swat2012_weather_worker = ImportSwat2012WeatherWorker()
        self.import_swat2012_weather_worker.moveToThread(self.import_swat2012_weather_thread)
        self.import_swat2012_weather_worker.sig_done.connect(self.on_import_swat2012_weather_done)
        self.import_swat2012_weather_worker.sig_error.connect(self.show_program_fail_message_box)
        self.import_swat2012_weather_worker.sig_station_done.connect(self.on_weather_station_done)
        self.import_swat2012_weather_worker.sig_aborted.connect(self.on_import_swat2012_weather_aborted)
        self.import_swat2012_weather_thread.started.connect(self.import_swat2012_weather_worker.work)

    def start_import_swat2012_weather(self):
        self.import_swat2012_weather_worker.set_abort(False)
        self.import_swat2012_weather_thread.start()

        self.import_swat2012_weather_progdialog = QtWidgets.QProgressDialog("Importing SWAT2012 weather data...", "Cancel", 0, 100, self)
        self.import_swat2012_weather_progdialog.setFixedWidth(350)
        self.import_swat2012_weather_progdialog.setWindowTitle("Import weather data")
        self.import_swat2012_weather_progdialog.canceled.connect(lambda: self.import_swat2012_weather_worker.abort())
        self.import_swat2012_weather_progdialog.finished.connect(self.abort_import_swat2012_weather_worker)
        self.import_swat2012_weather_progdialog.setModal(True)

        self.import_swat2012_weather_progdialog.setValue(0)
        self.import_swat2012_weather_progdialog.exec_()

    def set_import_swat2012_weather_progdialog(self, msg, val):
        self.import_swat2012_weather_progdialog.setLabelText(msg)
        self.import_swat2012_weather_progdialog.setValue(val)

    @pyqtSlot(object)
    def on_weather_station_done(self, prog):
        self.set_import_swat2012_weather_progdialog(prog.msg, prog.val)

    @pyqtSlot()
    def on_import_swat2012_weather_done(self):
        self.import_swat2012_weather_progdialog.close()
        self.import_swat2012_weather_thread.quit()
        self.import_swat2012_weather_thread.wait()
        self.show_message_box("Weather Data Imported",
                              "Your SWAT2012 formatted weather data has been imported to SWAT+ format")

    @pyqtSlot()
    def on_import_swat2012_weather_aborted(self):
        self.import_swat2012_weather_progdialog.close()
        self.import_swat2012_weather_thread.quit()
        self.import_swat2012_weather_thread.wait()
        self.show_message_box("Weather Data Import Aborted",
                              "SWAT2012 weather data import has been cancelled. Some files may be partially written in the 'swatplus' directory.",
                              QtWidgets.QMessageBox.Warning)

    def abort_import_swat2012_weather_worker(self):
        self.import_swat2012_weather_progdialog.close()
        self.import_swat2012_weather_thread.quit()
        self.import_swat2012_weather_thread.wait()
    # </editor-fold>

    # <editor-fold desc="ImportSwatPlusWeatherWorker">
    def connect_import_swatplus_weather_worker(self):
        self.import_swatplus_weather_thread = QThread()
        self.import_swatplus_weather_worker = ImportSwatPlusWeatherWorker()
        self.import_swatplus_weather_worker.moveToThread(self.import_swatplus_weather_thread)
        self.import_swatplus_weather_worker.sig_done.connect(self.on_import_swatplus_weather_done)
        self.import_swatplus_weather_worker.sig_error.connect(self.show_program_fail_message_box)
        self.import_swatplus_weather_worker.sig_aborted.connect(self.on_import_swatplus_weather_aborted)
        self.import_swatplus_weather_worker.sig_update_prog.connect(self.on_import_swatplus_weather_update)
        self.import_swatplus_weather_thread.started.connect(self.import_swatplus_weather_worker.work)

    def start_import_swatplus_weather(self):
        self.import_swatplus_weather_worker.set_abort(False)
        self.import_swatplus_weather_thread.start()

        self.import_swatplus_weather_progdialog = QtWidgets.QProgressDialog("Importing SWAT+ weather data...",
                                                                            "Cancel", 0, 100, self)
        self.import_swatplus_weather_progdialog.setFixedWidth(350)
        self.import_swatplus_weather_progdialog.setWindowTitle("Import weather data")
        self.import_swatplus_weather_progdialog.canceled.connect(lambda: self.import_swatplus_weather_worker.abort())
        self.import_swatplus_weather_progdialog.finished.connect(self.abort_import_swatplus_weather_worker)
        self.import_swatplus_weather_progdialog.setModal(True)

        self.import_swatplus_weather_progdialog.setValue(0)
        self.import_swatplus_weather_progdialog.exec_()

    def set_import_swatplus_weather_progdialog(self, msg, val):
        self.import_swatplus_weather_progdialog.setLabelText(msg)
        self.import_swatplus_weather_progdialog.setValue(val)

    @pyqtSlot(object)
    def on_import_swatplus_weather_update(self, prog):
        self.set_import_swatplus_weather_progdialog(prog.msg, prog.val)

    @pyqtSlot()
    def on_import_swatplus_weather_done(self):
        self.import_swatplus_weather_progdialog.close()
        self.import_swatplus_weather_thread.quit()
        self.import_swatplus_weather_thread.wait()
        self.show_message_box("Weather Data Imported",
                              "Your SWAT+ formatted weather data has been imported")

    @pyqtSlot()
    def on_import_swatplus_weather_aborted(self):
        self.import_swatplus_weather_progdialog.close()
        self.import_swatplus_weather_thread.quit()
        self.import_swatplus_weather_thread.wait()
        self.show_message_box("Weather Data Import Aborted",
                              "SWAT+ weather data import has been cancelled.",
                              QtWidgets.QMessageBox.Warning)

    def abort_import_swatplus_weather_worker(self):
        self.import_swatplus_weather_progdialog.close()
        self.import_swatplus_weather_thread.quit()
        self.import_swatplus_weather_thread.wait()
    # </editor-fold>

    # <editor-fold desc="WeatherStations">
    def import_weather_data(self):
        if self.weatherDirectory.text() == '':
            self.show_message_box("Select a weather directory",
                                  "Please select the directory containing your weather data files.",
                                  QtWidgets.QMessageBox.Critical)
        elif not os.path.exists(self.weatherDirectory.text()):
            self.show_message_box("Invalid weather directory",
                                  "The directory you entered does not exist. Please select a valid directory and try again.",
                                  QtWidgets.QMessageBox.Critical)
        elif self.weatherImportFormat.currentIndex() == 0:
            self.show_message_box("Select your file format",
                                  "Please select the format of your weather data files.",
                                  QtWidgets.QMessageBox.Critical)
        elif self.weatherImportFormat.currentIndex() == 1:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Import SWAT+ weather data")
            msg.setText("We will create SWAT+ weather stations based on your data, and match the closed station to your watershed elements. Your original files will not be changed. This may take several minutes to complete. Click 'OK' to continue.")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
            reply = msg.exec_()

            if reply == QtWidgets.QMessageBox.Ok:
                saved = Project_config.update_weather_data(self.weatherDirectory.text(), self.weatherImportFormat.currentText())
                if saved == 1:
                    self.set_saved_changes_success(self.weatherStationsStatusIcon, self.weatherStationsStatusLabel, "Weather stations")
                    self.start_import_swatplus_weather()
                else:
                    self.set_saved_changes_error(self.weatherStationsStatusIcon, self.weatherStationsStatusLabel)
            else:
                self.show_message_box("Error saving to database",
                                      "There was a problem updating your weather data settings in your project database.",
                                      QtWidgets.QMessageBox.Critical)
        elif self.weatherImportFormat.currentIndex() == 2:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Import SWAT2012 weather data")
            msg.setText("We will create the folder 'swatplus' in the directory provided and convert your SWAT2012 files to SWAT+ format. Your original files will not be changed. This may take several minutes to complete. Click 'OK' to continue.")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
            reply = msg.exec_()

            if reply == QtWidgets.QMessageBox.Ok:
                saved = Project_config.update_weather_data(self.weatherDirectory.text(), self.weatherImportFormat.currentText())
                if saved == 1:
                    self.set_saved_changes_success(self.weatherStationsStatusIcon, self.weatherStationsStatusLabel, "Weather stations")
                    self.start_import_swat2012_weather()
                else:
                    self.set_saved_changes_error(self.weatherStationsStatusIcon, self.weatherStationsStatusLabel)
            else:
                self.show_message_box("Error saving to database",
                                      "There was a problem updating your weather data settings in your project database.",
                                      QtWidgets.QMessageBox.Critical)
    # </editor-fold>

    # <editor-fold desc="ImportSwatPlusWeatherWorker">

    def connect_import_wgn_worker(self):
        self.import_wgn_thread = QThread()
        self.import_wgn_worker = ImportWgnWorker()
        self.import_wgn_worker.moveToThread(self.import_wgn_thread)
        self.import_wgn_worker.sig_done.connect(self.on_import_wgn_done)
        self.import_wgn_worker.sig_error.connect(self.show_program_fail_message_box)
        self.import_wgn_worker.sig_aborted.connect(self.on_import_wgn_aborted)
        self.import_wgn_worker.sig_update_prog.connect(self.on_import_wgn_update)
        self.import_wgn_thread.started.connect(self.import_wgn_worker.work)

    def start_import_wgn(self):
        self.import_wgn_worker.set_abort(False)
        self.import_wgn_thread.start()

        self.import_wgn_progdialog = QtWidgets.QProgressDialog("Importing weather generator data...",
                                                                            "Cancel", 0, 100, self)
        self.import_wgn_progdialog.setFixedWidth(350)
        self.import_wgn_progdialog.setWindowTitle("Import wgn data")
        self.import_wgn_progdialog.canceled.connect(lambda: self.import_wgn_worker.abort())
        self.import_wgn_progdialog.finished.connect(self.abort_import_wgn_worker)
        self.import_wgn_progdialog.setModal(True)

        self.import_wgn_progdialog.setValue(0)
        self.import_wgn_progdialog.exec_()

    def set_import_wgn_progdialog(self, msg, val):
        self.import_wgn_progdialog.setLabelText(msg)
        self.import_wgn_progdialog.setValue(val)

    @pyqtSlot(object)
    def on_import_wgn_update(self, prog):
        self.set_import_wgn_progdialog(prog.msg, prog.val)

    @pyqtSlot()
    def on_import_wgn_done(self):
        self.import_wgn_progdialog.close()
        self.import_wgn_thread.quit()
        self.import_wgn_thread.wait()
        self.show_message_box("Weather Generator Data Imported",
                              "Your weather generator data has been imported")

    @pyqtSlot()
    def on_import_wgn_aborted(self):
        self.import_wgn_progdialog.close()
        self.import_wgn_thread.quit()
        self.import_wgn_thread.wait()
        self.show_message_box("Weather Generator Data Import Aborted",
                              "Weather generator data import has been cancelled.",
                              QtWidgets.QMessageBox.Warning)

    def abort_import_wgn_worker(self):
        self.import_wgn_progdialog.close()
        self.import_wgn_thread.quit()
        self.import_wgn_thread.wait()
    # </editor-fold>

    # <editor-fold desc="Wgn">
    def add_wgn_to_project(self):
        if self.wgnDb.text() == '':
            self.show_message_box("Select a wgn database",
                                  "Please select the SQLite database file containing your weather generator data.",
                                  QtWidgets.QMessageBox.Critical)
        elif self.wgnDbTable.currentText() == '':
            self.show_message_box("Select a wgn database table",
                                  "Please select the weather generator table name from your SQLite database file.",
                                  QtWidgets.QMessageBox.Critical)
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Add wgn data to project")
            msg.setText(
                "This will replace all weather generator data for this project and associate it with the closed weather station. Click 'OK' to continue.")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
            reply = msg.exec_()

            if reply == QtWidgets.QMessageBox.Ok:
                saved = Project_config.update_wgn(self.wgnDb.text(), self.wgnDbTable.currentText())
                if saved == 1:
                    self.set_saved_changes_success(self.wgnStatusIcon, self.wgnStatusLabel,
                                                   "WGN")
                    self.start_import_wgn()
                else:
                    self.set_saved_changes_error(self.wgnStatusIcon, self.wgnStatusLabel)
            else:
                self.show_message_box("Error saving to database",
                                      "There was a problem updating your wgn settings in your project database.",
                                      QtWidgets.QMessageBox.Critical)

    def load_wgn_tables(self):
        self.wgnDbTable.clear()
        if os.path.exists(self.wgnDb.text()):
            conn = sqlite3.connect(self.wgnDb.text())
            query = "SELECT name FROM sqlite_master WHERE type='table'"
            cursor = conn.cursor().execute(query)

            for row in cursor.fetchall():
                self.wgnDbTable.addItem(row[0])

    # </editor-fold>

    # <editor-fold desc="WriteInputFilesWorker">
    def connect_write_files_worker(self):
        self.write_files_thread = QThread()
        self.write_files_worker = WriteInputFilesWorker()
        self.write_files_worker.moveToThread(self.write_files_thread)

        self.write_files_worker.sig_done.connect(self.on_write_files_done)
        self.write_files_worker.sig_error.connect(self.show_program_fail_message_box)
        self.write_files_worker.sig_aborted.connect(self.on_write_files_aborted)
        self.write_files_worker.sig_update_prog.connect(self.on_write_files_update)

        self.write_files_thread.started.connect(self.write_files_worker.work)

    def start_write_files(self):
        self.write_files_worker.set_abort(False)
        self.write_files_thread.start()

        self.write_files_progdialog = QtWidgets.QProgressDialog("Writing SWAT+ input files...", "Cancel", 0, 100, self)
        self.write_files_progdialog.setFixedWidth(350)
        self.write_files_progdialog.setWindowTitle("Writing SWAT+ input files")
        self.write_files_progdialog.canceled.connect(lambda: self.write_files_worker.abort())
        self.write_files_progdialog.finished.connect(self.abort_write_files_worker)
        self.write_files_progdialog.setModal(True)

        self.write_files_progdialog.setValue(0)
        self.write_files_progdialog.exec_()

    def set_write_files_progdialog(self, msg, val):
        self.write_files_progdialog.setLabelText(msg)
        self.write_files_progdialog.setValue(val)

    @pyqtSlot(object)
    def on_write_files_update(self, prog):
        self.set_write_files_progdialog(prog.msg, prog.val)

    @pyqtSlot()
    def on_write_files_done(self):
        self.write_files_progdialog.close()
        self.write_files_thread.quit()
        self.write_files_thread.wait()
        self.show_message_box("Files written",
                              "Your SWAT+ input files have been written")

    @pyqtSlot()
    def on_write_files_aborted(self):
        self.write_files_progdialog.close()
        self.write_files_thread.quit()
        self.write_files_thread.wait()
        self.show_message_box("Writing Input Files Aborted",
                              "SWAT+ input file writing has been cancelled.",
                              QtWidgets.QMessageBox.Warning)

    def abort_write_files_worker(self):
        self.write_files_progdialog.close()
        self.write_files_thread.quit()
        self.write_files_thread.wait()
    # </editor-fold>

    # <editor-fold desc="WeatherStations">
    def write_files(self):
        if self.inputFilesDir.text() == '':
            self.show_message_box("Select an input files directory",
                                  "Please select the directory where you want us to write your SWAT+ input files.",
                                  QtWidgets.QMessageBox.Critical)
        elif not os.path.exists(self.inputFilesDir.text()):
            self.show_message_box("Invalid input files directory",
                                  "The directory you entered does not exist. Please select a valid directory and try again.",
                                  QtWidgets.QMessageBox.Critical)
        else:
            msg = QtWidgets.QMessageBox()
            msg.setIcon(QtWidgets.QMessageBox.Warning)
            msg.setWindowTitle("Write SWAT+ input files")
            msg.setText(
                "Any existing files will be overwritten. This may take several minutes to complete. Click 'OK' to continue.")
            msg.setStandardButtons(QtWidgets.QMessageBox.Ok | QtWidgets.QMessageBox.Cancel)
            reply = msg.exec_()

            if reply == QtWidgets.QMessageBox.Ok:
                saved = Project_config.update_input_files_dir(self.inputFilesDir.text())
                if saved == 1:
                    self.start_write_files()
                else:
                    self.show_message_box("Error saving to database",
                                          "There was a problem updating your input files directory setting in your project database.",
                                          QtWidgets.QMessageBox.Critical)
    # </editor-fold>

    # <editor-fold desc="Helpers">
    def set_unsaved_changes_warning(self, icon, label):
        icon.setPixmap(QtGui.QPixmap(":/gui/resources/icons/warn.png"))
        icon.setVisible(True)
        label.setStyleSheet("color: #cb8015")
        label.setText("You have unsaved changes on this page.")
        label.setVisible(True)

    def set_saved_changes_success(self, icon, label, section_name):
        icon.setPixmap(QtGui.QPixmap(":/gui/resources/icons/success.png"))
        icon.setVisible(True)
        label.setStyleSheet("color: #008800")
        label.setText(
            "{name} data saved {time}".format(name=section_name, time=datetime.datetime.now().strftime("%b %d, %Y %I:%M %p")))
        label.setVisible(True)

    def set_saved_changes_error(self, icon, label):
        icon.setPixmap(QtGui.QPixmap(":/gui/resources/icons/critical.png"))
        icon.setVisible(True)
        label.setStyleSheet("color: #cc0000")
        label.setText("Error saving data to project database. Ensure there is not a problem with the file and try again.")
        label.setVisible(True)

    def get_full_file_path(self, file_name, cwd):
        if file_name == "":
            return ""
        elif os.path.exists(file_name):
            return file_name
        elif os.path.exists(os.path.join(cwd, file_name)):
            return os.path.join(cwd, file_name)
        else:
            self.show_message_box("File not found", "Could not locate %s" % file_name)

    def show_message_box(self, title, message, icon=QtWidgets.QMessageBox.Information):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(icon)
        msg.setWindowTitle(title)
        msg.setText(message)
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()

    def show_program_fail_message_box(self, e):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setWindowTitle("Unspecified error")
        msg.setText(
            "SWAT+ Editor encountered an unexpected error. Click 'Show details...' and report this message to technical support at %s." % tech_support_email)
        msg.setDetailedText(str(e))
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        msg.exec_()
    # </editor-fold>

    end_fold = 'pycharm bug'


def main():
    app = QtWidgets.QApplication(sys.argv)
    form = SWATPlusEditor()
    form.show()
    app.exec_()