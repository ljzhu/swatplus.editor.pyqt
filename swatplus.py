import sys
import editor.main

from database import gis, wgn
from database.soils import ImportSoils
from database.wgn import SetupWgnDatabase, ImportWgn

from database.datasets import base as datasets_base
from database.datasets.setup import SetupDatasetsDatabase

#from database.soils_tests import SoilTests


def trap_exc_during_debug(*args):
    # when app raises uncaught exception, print info
    print(args)


# install exception hook: without this, uncaught exception would cause application to exit
sys.excepthook = trap_exc_during_debug


def convert_soils(gis_db, orig_table):
    gis.db.init(gis_db)
    if gis.Soil.select().count() < 1:
        ImportSoils.insert(gis_db, orig_table, gis.Soil, gis.Soil_layer, True, 1, False, gis.db)
    # when done: update hrus set soil_id = (select id from soil where soil.name = hrus.SOIL) where exists(select * from soil where soil.name = hrus.SOIL)


def setup_wgn(wgn_db):
    wgn.db.init(wgn_db)
    SetupWgnDatabase.create_tables()


def convert_wgn(wgn_db, wgn_table, wgn_monthly_value_table, orig_db, orig_table):
    wgn.db.init(wgn_db)
    ImportWgn.insert(orig_db, orig_table, wgn_table, wgn_monthly_value_table, 1, True, wgn.db)


def add_wgn():
    wgn_db = "D:\Clouds\OneDrive\Documents\Work\SWATPlus\Code\Python\swatplus\data\example-project\swatplus_wgn.sqlite"
    setup_wgn(wgn_db)
    convert_wgn(wgn_db, wgn.Wgn_cfsr_world, wgn.Wgn_cfsr_world_mon,
                "C:/Users/jaclyn-b-tech/Downloads/cfsr_world/CFSR_World.mdb", "WGEN_CFSR_World")
    convert_wgn(wgn_db, wgn.Wgn_us, wgn.Wgn_us_mon, "C:\SWAT\SWATEditor\Databases\SWAT2012.mdb",
                "WGEN_US_FirstOrder")


def init_datasets_db(datasets_db):
    datasets_base.db.init(datasets_db)
    SetupDatasetsDatabase.create_tables()
    SetupDatasetsDatabase.initialize_data()


if __name__ == "__main__":
    #init_datasets_db("data/example-project/swatplus_datasets.sqlite")
    #st = SoilTests("D:\Clouds\OneDrive\Documents\Work\SWATPlus\Docs\swatplus_soils_tests.sqlite", "norm.txt", "norm2.txt")
    #st.write_norm_text_file(2)
    #st.write_norm_text_file_join(2)

    editor.main.main()
